#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
        
    def test_solve_case_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\nD [dead]\n")
        
    def test_solve_case_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_case_4(self):
        r = StringIO("A Tokyo Hold\nB Osaka Move Tokyo\nC Kyoto Support B\nD Nagoya Move Tokyo\nE Fukuoka Support D\nF Sapporo Move Osaka")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Kyoto\nD [dead]\nE Fukuoka\nF Osaka\n")

    def test_solve_case_5(self):
        r = StringIO(
            "K Vienna Hold\n"
            "L Bucharest Move Vienna\n"
            "M Sofia Support L\n"
            "N Athens Move Bucharest\n"
            "O Tobruk Support N\n"
            "P Tripoli Move Athens\n"
            "Q Algiers Support P\n"
            "R Tunis Hold\n"
            "S Casablanca Move Tunis\n"
            "T Cairo Support S\n"
            "U Benghazi Move Cairo\n"
            "V Suez Support U\n"
            "W Rabat Move Suez\n"
            "X Oran Support W\n"
            "Y Alexandria Hold\n"
            "Z Gibraltar Move Alexandria"
        )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "K [dead]\n"
            "L Vienna\n"
            "M Sofia\n"
            "N Bucharest\n"
            "O Tobruk\n"
            "P Athens\n"
            "Q Algiers\n"
            "R [dead]\n"
            "S [dead]\n"
            "T [dead]\n"
            "U [dead]\n"
            "V [dead]\n"
            "W Suez\n"
            "X Oran\n"
            "Y [dead]\n"
            "Z [dead]\n"
        )

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
