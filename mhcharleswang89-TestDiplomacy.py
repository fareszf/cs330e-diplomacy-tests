# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve

# ------------
# TestDiplomacy
# ------------

class TestDiplomacy(TestCase):
    # ----
    # solve
    # ----
    
    def run_diplomacy_solve(self, input_data):

        # Helper function to call diplomacy_solve with input_data.
        # Uses StringIO to simulate file-like input and output streams.

        input_stream = StringIO(input_data)
        output_stream = StringIO()
        diplomacy_solve(input_stream, output_stream)
        return output_stream.getvalue().strip()

    def test_solve_1(self):
        """
        Test scenario where all players end up dead due to conflicting moves.
        """
        input_data = """\
A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Austin Move London"""
        expected_output = """\
A [dead]
B [dead]
C [dead]
D [dead]"""
        output = self.run_diplomacy_solve(input_data)
        self.assertEqual(output, expected_output)

    def test_solve_2(self):
        """
        Test scenario where no conflicts occur, and all players hold their positions.
        """
        input_data = """\
A Madrid Hold
B Barcelona Hold
C London Hold
D Austin Hold"""
        expected_output = """\
A Madrid
B Barcelona
C London
D Austin"""
        output = self.run_diplomacy_solve(input_data)
        self.assertEqual(output, expected_output)

    def test_solve_3(self):
        """
        Test scenario where one player moves with support, resulting in conflict resolution.
        """
        input_data = """\
A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Austin Hold"""
        expected_output = """\
A [dead]
B Madrid
C London
D Austin"""
        output = self.run_diplomacy_solve(input_data)
        self.assertEqual(output, expected_output)

    def test_solve_4(self):
        """
        Test scenario where support succeeds and no other player attacks the supporting unit.
        """
        input_data = """\
A Madrid Hold
B Barcelona Move Madrid
C London Support B
D Austin Hold"""
        expected_output = """\
A [dead]
B Madrid
C London
D Austin"""
        output = self.run_diplomacy_solve(input_data)
        self.assertEqual(output, expected_output)


    def test_solve_5(self):
            """
            Invalid action
            """
            input_data = """\
    A Madrid Hold
    B Nashville Attack Madrid
    C London Support B
    D Austin Hold"""
            with self.assertRaises(AssertionError):
                self.run_diplomacy_solve(input_data)


# ----
# main
# ----

if __name__ == "__main__":
    main()
