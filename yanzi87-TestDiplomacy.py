import unittest
from Diplomacy import *
from io import StringIO

class TestGetBattles(unittest.TestCase):
    def test_case1(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
        ]
        expectedResult = ['Madrid']
        result = getBattles(allContent)
        self.assertEqual(result, expectedResult)

    def test_case2(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Support', 'B'],
        ]
        expectedResult = ['Madrid', 'London']
        result = getBattles(allContent)
        self.assertEqual(result, expectedResult)

    def test_case3(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
        ]
        expectedResult = ['Madrid']
        result = getBattles(allContent)
        self.assertEqual(result, expectedResult)

    def test_case4(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Support', 'B'],
            ['D', 'Austin', 'Move', 'London'],
        ]
        expectedResult = ['Madrid', 'London']
        result = getBattles(allContent)
        self.assertEqual(result, expectedResult)

    def test_case5(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
        ]
        expectedResult = ['Madrid']
        result = getBattles(allContent)
        self.assertEqual(result, expectedResult)

    def test_case6(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
            ['D', 'Paris', 'Support', 'B'],
        ]
        expectedResult = ['Madrid', 'Paris']
        result = getBattles(allContent)
        self.assertEqual(result, expectedResult)

    def test_case7(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
            ['D', 'Paris', 'Support', 'B'],
            ['E', 'Austin', 'Support', 'A'],
        ]
        expectedResult = ['Madrid', 'Paris', 'Austin']
        result = getBattles(allContent)
        self.assertEqual(result, expectedResult)

class TestGetSupport(unittest.TestCase):
    def assertUnorderedListOfListsEqual(self, list1, list2):
        self.assertEqual(len(list1), len(list2), "Outer lists are of different lengths.")
        sorted_list1 = sorted([sorted(sublist) for sublist in list1])
        sorted_list2 = sorted([sorted(sublist) for sublist in list2])
        self.assertEqual(sorted_list1, sorted_list2, "Lists do not contain the same elements.")


    def test_case1(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
        ]
        expectedResult = [['A']]
        result = getSupport('Madrid', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case2_madrid(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Support', 'B'],
        ]
        expectedResult = [['A'], ['B', 'C']]
        result = getSupport('Madrid', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case2_london(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Support', 'B'],
        ]
        expectedResult = [['C']]
        result = getSupport('London', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case3(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
        ]
        expectedResult = [['A'], ['B']]
        result = getSupport('Madrid', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case4_madrid(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Support', 'B'],
            ['D', 'Austin', 'Move', 'London'],
        ]
        expectedResult = [['A'], ['B']]
        result = getSupport('Madrid', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case4_london(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Support', 'B'],
            ['D', 'Austin', 'Move', 'London'],
        ]
        expectedResult = [['C'], ['D']]
        result = getSupport('London', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case5(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
        ]
        expectedResult = [['A'], ['B'], ['C']]
        result = getSupport('Madrid', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case6_madrid(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
            ['D', 'Paris', 'Support', 'B'],
        ]
        expectedResult = [['A'], ['C'], ['B', 'D']]
        result = getSupport('Madrid', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case6_paris(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
            ['D', 'Paris', 'Support', 'B'],
        ]
        expectedResult = [['D']]
        result = getSupport('Paris', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case7_madrid(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
            ['D', 'Paris', 'Support', 'B'],
            ['E', 'Austin', 'Support', 'A'],
        ]
        expectedResult = [['A', 'E'], ['B', 'D'], ['C']]
        result = getSupport('Madrid', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case7_paris(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
            ['D', 'Paris', 'Support', 'B'],
            ['E', 'Austin', 'Support', 'A'],
        ]
        expectedResult = [['D']]
        result = getSupport('Paris', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

    def test_case7_austin(self):
        allContent = [
            ['A', 'Madrid', 'Hold'],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Move', 'Madrid'],
            ['D', 'Paris', 'Support', 'B'],
            ['E', 'Austin', 'Support', 'A'],
        ]
        expectedResult = [['E']]
        result = getSupport('Austin', allContent)
        self.assertUnorderedListOfListsEqual(result, expectedResult)

class test_getDead (unittest.TestCase):

  def test_getDead1 (self):
      self.assertEqual(getDead([['A']]), [])
  def test_getDead2 (self):
      self.assertEqual(getDead([['A'],['B', 'C']]), ['A'])
  def test_getDead3 (self):
      self.assertEqual(getDead([["A"], ["B"]]), ["A", "B"])
  def test_getDead4 (self):
      self.assertEqual(getDead([["C"], ["D"]]), ["C", "D"])
  def test_getDead5 (self):
      self.assertEqual(getDead([["A"], ["B"], ["C"]]), ["A", "B", "C"])
  def test_getDead6 (self):
      self.assertEqual(getDead([["A"], ["C"], ["B", "D"]]), ["A", "C"])
  def test_getDead7 (self):
      self.assertEqual(getDead([["A", "E"], ["B", "D"], ["C"]]), ["A", "B", "C"])


class test_getFinalStatus (unittest.TestCase):

  def test_final1 (self):
      actions = [('A', 'Madrid', 'Hold')]
      dead_list = []
      expected_result = {'A': 'Madrid'}
      self.assertEqual(getFinalStatus(actions, dead_list), expected_result)

  def test_final2 (self):
      actions = [
          ('A', 'Madrid', 'Hold'),
          ('B', 'Barcelona', 'Move Madrid'),
          ('C', 'London', 'Support B')
      ]
      dead_list = ["A"]
      expected_result = {"A": "[dead]", "B": "Move Madrid", "C": "Support B"}
      self.assertEqual(getFinalStatus(actions, dead_list), expected_result)

  def test_final3 (self):
      actions = [
          ("A", "Madrid", "Hold"),
          ("B", "Barcelona", "Move Madrid")
      ]
      dead_list = ["A", "B"]
      expected_result = {"A": "[dead]", "B": "[dead]"}
      self.assertEqual(getFinalStatus(actions, dead_list), expected_result)

  def test_final4 (self):
      actions = [
          ("A", "Madrid", "Hold"),
          ("B", "Barcelona", "Move Madrid"),
          ("C", "London", "Support B"),
          ("D", "Austin", "Move London")
      ]
      dead_list = ["A", "B", "C", "D"]
      expected_result = {"A": "[dead]", "B": "[dead]", "C": "[dead]", "D": "[dead]"}
      self.assertEqual(getFinalStatus(actions, dead_list), expected_result)

  def test_final5 (self):
      actions = [
          ("A", "Madrid", "Hold"),
          ("B", "Barcelona", "Move Madrid"),
          ("C", "London", "Move Madrid")
      ]
      dead_list = ["A", "B", "C"]
      expected_result = {"A": "[dead]", "B": "[dead]", "C": "[dead]"}
      self.assertEqual(getFinalStatus(actions, dead_list), expected_result)

  def test_final6 (self):
      actions = [
          ("A", "Madrid", "Hold"),
          ("B", "Barcelona", "Move Madrid"),
          ("C", "London", "Move Madrid"),
          ("D", "Paris", "Support B")
      ]
      dead_list = ["A", "C"]
      expected_result = {"A": "[dead]", "B": "Move Madrid", "C": "[dead]", "D": "Support B"}
      self.assertEqual(getFinalStatus(actions, dead_list), expected_result)


# unit tests for diplomacy solve
class TestDiplomacySolve(unittest.TestCase):

    def test_solve_0(self):
        r = StringIO("")
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve_1(self):
        r = StringIO("\n\n")
        w = StringIO("")
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(),"")


    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        expected_output = "A [dead]\nB Madrid\nC London\n"
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_output)


    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        expected_output = "A [dead]\nB [dead]\n"
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_output)


    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        expected_output = "A [dead]\nB Madrid\nC [dead]\nD Paris\n"
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_output)


    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        expected_output = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        w = StringIO("")
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), expected_output)


if __name__ == '__main__': #pragma: no cover
    unittest.main()
