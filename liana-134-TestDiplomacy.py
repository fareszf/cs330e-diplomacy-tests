#!/usr/bin/env python3

# TestDiplomacy.py
# Write unit tests that test corner cases and failure cases until you have 3 tests for the function diplomacy_solve()
# confirm the expected failures, and add, commit, and push to the private code repo.

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):
    def test_solve1(self):
            r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")

            w = StringIO()
            diplomacy_solve(r, w)
            self.assertEqual(
                w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
             
    def test_solve2(self):
            r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")

            w = StringIO()
            diplomacy_solve(r, w)
            self.assertEqual(
                w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve3(self):
            r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")

            w = StringIO()
            diplomacy_solve(r, w)
            self.assertEqual(
                w.getvalue(), "A [dead]\nB [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

