from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # Read tests
    def test_read_one(self):
        s = "A Austin Hold\n"
        val = diplomacy_read(s)
        self.assertEqual(val, ["A", "Austin", "Hold"])

    def test_read_two(self):
        s = "A Austin Move Norman\n"
        val = diplomacy_read(s)
        self.assertEqual(val, ["A", "Austin", "Move", "Norman"])

    def test_read_three(self):
        s = "A Austin Support B\n"
        val = diplomacy_read(s)
        self.assertEqual(val, ["A", "Austin", "Support", "B"])

    # Input tests
    def test_hold(self):
        s = "A Madrid Hold\n"
        inp_str = diplomacy_read(s)
        self.assertEqual(inp_str, ["A", "Madrid", "Hold"])

    def test_move(self):
        s = "B Barcelona Move Madrid"
        inp_str = diplomacy_read(s)
        self.assertEqual(inp_str, ["B", "Barcelona", "Move", "Madrid"])
    
    def test_support(self):
        s = "D Paris Support B"
        inp_str = diplomacy_read(s)
        self.assertEqual(inp_str, ["D", "Paris", "Support", "B"])

    # Output tests
    def test_print_base(self):
        w = StringIO()
        diplomacy_print(w, [["A", "Madrid"]])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_dead(self):
        w = StringIO()
        diplomacy_print(w, [["A", "[dead]"]])
        self.assertEqual(w.getvalue(), "A [dead]\n")

    # Solve tests
    def test_solve_one(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
    def test_solve_two(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_three(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()