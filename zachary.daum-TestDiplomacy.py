from unittest import TestCase, main
from Diplomacy import diplomacy_solve

import io

class TestCollatz (TestCase):
    # ----------------
    # diplomacy_solve
    # ----------------

    def test_diplomacy_solve_1(self):
        r = io.StringIO("A Madrid Hold\n")
        w = io.StringIO()
        diplomacy_solve(r, w)
        a = "A Madrid\n"
        self.assertEqual(w.getvalue(), a)

    def test_diplomacy_solve_2(self):
        r = io.StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = io.StringIO()
        diplomacy_solve(r, w)
        a = "A [dead]\nB Madrid\nC London\n"
        self.assertEqual(w.getvalue(), a)

    def test_diplomacy_solve_3(self):
        r = io.StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = io.StringIO()
        diplomacy_solve(r, w)
        a = "A [dead]\nB [dead]\n"
        self.assertEqual(w.getvalue(), a)

    def test_diplomacy_solve_4(self):
        r = io.StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = io.StringIO()
        diplomacy_solve(r, w)
        a = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        self.assertEqual(w.getvalue(), a)

    def test_diplomacy_solve_5(self):
        r = io.StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = io.StringIO()
        diplomacy_solve(r, w)
        a = "A [dead]\nB [dead]\nC [dead]\n"
        self.assertEqual(w.getvalue(), a)

    def test_diplomacy_solve_6(self):
        r = io.StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = io.StringIO()
        diplomacy_solve(r, w)
        a = "A [dead]\nB Madrid\nC [dead]\nD Paris\n"
        self.assertEqual(w.getvalue(), a)

    def test_diplomacy_solve_7(self):
        r = io.StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = io.StringIO()
        diplomacy_solve(r, w)
        a = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n"
        self.assertEqual(w.getvalue(), a)

    def test_diplomacy_solve_8(self):
        r = io.StringIO("A Madrid Move Barcelona\n")
        w = io.StringIO()
        diplomacy_solve(r, w)
        a = "A Barcelona\n"
        self.assertEqual(w.getvalue(), a)

    def test_diplomacy_solve_9(self):
        r = io.StringIO("A Madrid Hold\nB Madrid Hold")
        w = io.StringIO()
        diplomacy_solve(r, w)
        a = "A [dead]\nB [dead]\n"
        self.assertEqual(w.getvalue(), a)


if __name__ == "__main__": #pragma: no cover
    main()