from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy(TestCase):
    def test_solve_1(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\n')
        
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')
        
    def test_solve_2(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n')
        
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\n')
        
    def test_solve_3(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n')
        
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')

    # Tests for Defender Reciving support
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\n")

        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC London\n")
    # Tests for Support Cancel
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Austin Move London")

        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # Tests for Different Support Cancel case
    def test_solve_6(self):
        r = StringIO("A Madrid Support C\nB Barcelona Move Madrid\nC London Support A\n")

        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC London\n")

    def test_solve_7(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid\nC London Support B\nD Austin Support B\n")

        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\nD Austin\n")

    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")

        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_solve_9(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Austin\n")

        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Austin\n")

    def test_solve_10(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Support B\nE Austin Move Paris\n")

        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC London\nD [dead]\nE [dead]\n")

    def test_solve_11(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Tokyo Move Austin\nF Fresno Hold\n")

        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE Austin\nF Fresno\n")

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()