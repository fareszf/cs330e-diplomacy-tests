#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy(TestCase):

    # ----
    # solve
    # ----
    def test_solve_1(self):
        r = StringIO("A Atlantis Hold\nB Elba Move Atlantis\nC Avalon Support A\nD Cydonia Move Avalon")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Atlantis\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Atlantis Hold\nB Cydonia Move Atlantis")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Avalon Move Atlantis\nB Cydonia Support A")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Atlantis\nB Cydonia\n")

    def test_solve_4(self):
        r = StringIO("A Atlantis Hold\nB Cydonia Move Eden\nC Avalon Support B")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Atlantis\nB Eden\nC Avalon\n")

    def test_solve_5(self):
        r = StringIO("A Atlantis Hold\nB Cydonia Move Atlantis\nC Avalon Support B\nD Eden Move Avalon")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Atlantis\nC [dead]\nD [dead]\n")

    def test_solve_6(self):
        r = StringIO("A Atlantis Hold\nB Cydonia Move Atlantis\nC Eden Support B")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Atlantis\nC Eden\n")

    def test_solve_7(self):
        r = StringIO("A Atlantis Hold\nB Cydonia Move Eden\nC Eden Support B")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Atlantis\nB Eden\nC [dead]\n")

    def test_solve_8(self):
        r = StringIO("A Atlantis Hold\nB Elba Move Eden\nC Cydonia Move Atlantis")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Eden\nC [dead]\n")

    def test_solve_9(self):
        r = StringIO("A Atlantis Move Cydonia\nB Elba Move Atlantis\nC Cydonia Support A")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Cydonia\nB Atlantis\nC [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()