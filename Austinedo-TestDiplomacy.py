from unittest import main, TestCase

from Diplomacy import diplomacy_solve
from io import StringIO

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    def test_diplomacy_solve1(self):
        r = StringIO("A London Hold\nB Paris Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\nB Paris\n")

    def test_diplomacy_solve2(self):
        r = StringIO("A London Hold\nB Paris Move London\nC Berlin Support B\nD Rome Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC Berlin\nD Rome\n")

    def test_diplomacy_solve3(self):
        r = StringIO("A London Hold\nB Paris Hold\nC Berlin Hold\nD Rome Move Berlin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A London\nB Paris\nC [dead]\nD [dead]\n")

    def test_diplomacy_solve4(self):
        r = StringIO("A Austin Hold\nB Seattle Move Austin\nC Chicago Support B\nD Dallas Move Chicago\nE Tampa Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Chicago\nD [dead]\nE Tampa\n")

    def test_diplomacy_solve5(self):
        r = StringIO('A Austin Hold\nB Dallas Move Austin\nC Houston Support B\nD Amarillo Move Houston')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

# ----
# main
# ----

if __name__ == "__main__":
    main()
