#!/usr/bin/env python3

# ------------------------------
# ./diplomacy/TestDiplomacy.py
# Ben & Rio
# ------------------------------

# -------
# imports
# -------

from unittest import main, TestCase
from collections import defaultdict
from Diplomacy import *
# To treat lines as a .in file
from io import StringIO


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_valid_input(self):
        input_data = "A Madrid Hold\nB Barcelona Move Madrid\n"
        expected = ["A Madrid Hold", "B Barcelona Move Madrid"]
        self.assertEqual(parse_input(StringIO(input_data)), expected)

    def test_invalid_command_format(self):
        input_data = "A Madrid Hold\nB Move Madrid\n"
        with self.assertRaises(AssertionError):
            parse_input(StringIO(input_data))

    def test_invalid_action_type(self):
        input_data = "A Madrid Fly\nB Barcelona Move Madrid\n"
        with self.assertRaises(AssertionError):
            parse_input(StringIO(input_data))

    def test_parse_input_valid(self):
        valid_input = "A Paris Move London\nB Berlin Hold"
        input_stream = StringIO(valid_input)
        result = parse_input(input_stream)
        self.assertEqual(result, ["A Paris Move London", "B Berlin Hold"])

    def test_parse_input_invalid_command_length(self):
        invalid_input = "A Paris\nB Berlin Hold"
        input_stream = StringIO(invalid_input)
        with self.assertRaises(AssertionError):
            parse_input(input_stream)

    def test_parse_input_invalid_army(self):
        invalid_input = "AB Paris Move London"
        input_stream = StringIO(invalid_input)
        with self.assertRaises(AssertionError):
            parse_input(input_stream)

    def test_parse_input_invalid_action(self):
        invalid_input = "A Paris Attack London"
        input_stream = StringIO(invalid_input)
        with self.assertRaises(AssertionError):
            parse_input(input_stream)

    def test_parse_commands_with_all_actions(self):
        input_data = """A Paris Hold
                        B London Move Madrid
                        C Rome Support B"""

        input_stream = StringIO(input_data)
        output_stream = StringIO()

        # Simulate the diplomacy_solve function
        commands = parse_input(input_stream)
        army_locations, actions, supports, conflicts, outcomes = initialize_data_structures()
        parse_commands(commands, army_locations, actions, supports, conflicts)

        # Ensure all actions are parsed correctly
        self.assertEqual(actions['A'], ('Hold', 'Paris'))
        self.assertEqual(actions['B'], ('Move', 'Madrid'))
        self.assertEqual(actions['C'], ('Support', 'B'))

        # Assert locations
        self.assertEqual(army_locations['A'], 'Paris')
        self.assertEqual(army_locations['B'], 'London')
        self.assertEqual(army_locations['C'], 'Rome')

        # Confirm conflicts and supports
        self.assertIn('B', conflicts['Madrid'])
        self.assertIn('B', supports)
        self.assertIn('C', supports['B'])

    # ----
    # structs
    # ----

    def test_data_structures(self):
        army_locations, actions, supports, conflicts, outcomes = initialize_data_structures()
        self.assertIsInstance(army_locations, dict)
        self.assertIsInstance(actions, dict)
        self.assertIsInstance(supports, defaultdict)
        self.assertIsInstance(conflicts, defaultdict)
        self.assertIsInstance(outcomes, dict)

    def test_empty_structures(self):
        army_locations, actions, supports, conflicts, outcomes = initialize_data_structures()
        self.assertEqual(army_locations, {})
        self.assertEqual(actions, {})
        self.assertEqual(supports, {})
        self.assertEqual(conflicts, {})
        self.assertEqual(outcomes, {})

    def test_defaultdict_types(self):
        _, _, supports, conflicts, _ = initialize_data_structures()
        self.assertIsInstance(supports, defaultdict)
        self.assertIsInstance(conflicts, defaultdict)

    # # ----
    # # commands
    # # ----

    def test_valid_commands(self):
        commands = ["A Madrid Hold", "B Barcelona Move Madrid"]
        army_locations, actions, supports, conflicts = {
        }, {}, defaultdict(list), defaultdict(list)
        parse_commands(commands, army_locations, actions, supports, conflicts)
        self.assertEqual(army_locations, {'A': 'Madrid', 'B': 'Barcelona'})
        self.assertEqual(actions['A'], ('Hold', 'Madrid'))
        self.assertEqual(actions['B'], ('Move', 'Madrid'))

    def test_hold_commands(self):
        commands = ["A Madrid Hold", "B Barcelona Hold"]
        army_locations, actions, supports, conflicts = {
        }, {}, defaultdict(list), defaultdict(list)
        parse_commands(commands, army_locations, actions, supports, conflicts)
        self.assertEqual(conflicts['Madrid'], ['A'])
        self.assertEqual(conflicts['Barcelona'], ['B'])

    def test_move_and_support_commands(self):
        commands = ["A Madrid Hold",
                    "B Barcelona Move Madrid", "C London Support B"]
        army_locations, actions, supports, conflicts = {
        }, {}, defaultdict(list), defaultdict(list)
        parse_commands(commands, army_locations, actions, supports, conflicts)
        self.assertEqual(actions['B'], ('Move', 'Madrid'))
        self.assertEqual(actions['C'], ('Support', 'B'))
        self.assertIn('B', supports)
        self.assertIn('Madrid', conflicts)

    def test_parse_commands_single_army(self):
        commands = ["A Paris Hold", "B Berlin Move Paris"]
        army_locations, actions, supports, conflicts, outcomes = initialize_data_structures()
        parse_commands(commands, army_locations, actions, supports, conflicts)

        self.assertEqual(army_locations, {"A": "Paris", "B": "Berlin"})
        self.assertEqual(
            actions, {"A": ("Hold", "Paris"), "B": ("Move", "Paris")})
        self.assertEqual(supports, {})
        self.assertEqual(conflicts, {"Paris": ["A", "B"]})

    def test_parse_commands_multiple_armies_conflict(self):
        commands = ["A Paris Move London", "B Paris Move London"]
        army_locations, actions, supports, conflicts, outcomes = initialize_data_structures()
        parse_commands(commands, army_locations, actions, supports, conflicts)
        self.assertEqual(conflicts["London"], ["A", "B"])

    # # ----
    # # support
    # # ----

    def test_no_invalidation_if_no_conflict(self):
        """Test no invalidation if no conflict exists."""
        # Setup commands
        commands = ['A London Support B', 'B Madrid Hold', 'C Vegas Hold']

        # Define locations for each army
        locations = {'A': 'London', 'B': 'Madrid', 'C': 'Vegas'}

        # Define actions for each army
        actions = {
            'A': ('Support', 'B'),
            'B': ('Hold', 'Madrid'),
            'C': ('Hold', 'Vegas')
        }

        # Define the supports dictionary where army A supports army B
        supports = defaultdict(list, {'B': ['A']})

        # Define the conflicts dictionary showing the locations of the armies
        conflicts = defaultdict(list, {
            'London': ['A'],
            'Madrid': ['B'],
            'Vegas': ['C']
        })

        # Before calling invalidate_supports, 'B' should be supported by 'A'
        self.assertEqual(supports['B'], ['A'])

        # Run the invalidate_supports function
        invalidate_supports(actions, supports, conflicts)

        # After invalidation, support for 'B' should still be 'A' because there is no conflict involving 'B'
        self.assertEqual(supports['B'], ['A'])

    def test_multiple_armies_in_conflict(self):
        """Test multiple armies in conflict invalidate supports."""
        actions = {
            'A': ('Support', 'B'),
            'B': ('Move', 'D'),
            'C': ('Support', 'B')
        }
        supports = defaultdict(list)
        conflicts = defaultdict(list)

        supports['B'] = ['A', 'C']
        conflicts['D'] = ['A', 'B', 'C']  # Three armies in conflict

        # Before invalidation
        self.assertEqual(supports['B'], ['A', 'C'])

        # Run the invalidate_supports function
        invalidate_supports(actions, supports, conflicts)

        # After invalidation: Both A and C should be removed from support
        self.assertEqual(supports['B'], [])

    def test_no_supports_to_invalidate(self):
        """Test when no supports need to be invalidated."""
        # Setup commands
        commands = ['A London Support B', 'B Madrid Hold']

        # Define locations for each army
        locations = {'A': 'London', 'B': 'Madrid'}

        # Define actions for each army
        actions = {
            'A': ('Support', 'B'),
            'B': ('Hold', 'Madrid')
        }

        # Define the supports dictionary where army A supports army B
        supports = defaultdict(list, {'B': ['A']})

        # Define the conflicts dictionary showing the locations of the armies
        conflicts = defaultdict(list, {
            'London': ['A'],
            'Madrid': ['B']
        })

        # Before calling invalidate_supports, 'B' should be supported by 'A'
        self.assertEqual(supports['B'], ['A'])

        # Run the invalidate_supports function
        invalidate_supports(actions, supports, conflicts)

        # After invalidation, support for 'B' should still be 'A' because there is no conflict involving 'B'
        self.assertEqual(supports['B'], ['A'])

    def test_invalidate_supports(self):
        actions = {"A": ("Support", "B"), "B": ("Hold", "Paris")}
        supports = defaultdict(list, {"B": ["A"]})
        conflicts = defaultdict(list, {"Paris": ["A", "B"]})

        invalidate_supports(actions, supports, conflicts)

        # A's support for B should be invalidated
        self.assertNotIn("A", supports["B"])

    # # ----
    # # conflicts
    # # ----

    def test_resolve_conflict_single_army(self):
        conflicts = {'Madrid': ['A']}
        supports = defaultdict(list)
        actions = {'A': ('Hold', 'Madrid')}
        outcomes = {}
        resolve_conflicts(conflicts, supports, actions, outcomes)
        self.assertEqual(outcomes, {'A': 'Madrid'})

    def test_resolve_conflict_tie_no_winner(self):
        conflicts = {'Madrid': ['A', 'B']}
        supports = defaultdict(list)
        actions = {'A': ('Move', 'Madrid'), 'B': ('Move', 'Madrid')}
        outcomes = {}
        resolve_conflicts(conflicts, supports, actions, outcomes)
        self.assertEqual(outcomes, {'A': '[dead]', 'B': '[dead]'})

    def test_resolve_conflict_clear_winner(self):
        conflicts = {'Madrid': ['A', 'B']}
        supports = defaultdict(list)
        supports['A'].append('C')  # C supports A
        actions = {'A': ('Move', 'Madrid'), 'B': (
            'Move', 'Madrid'), 'C': ('Support', 'A')}
        outcomes = {}
        resolve_conflicts(conflicts, supports, actions, outcomes)
        self.assertEqual(outcomes, {'A': 'Madrid', 'B': '[dead]'})

    def test_resolve_conflict_equal_support(self):
        conflicts = {'Madrid': ['A', 'B']}
        supports = defaultdict(list)
        supports['A'].append('C')
        supports['B'].append('D')
        actions = {'A': ('Move', 'Madrid'), 'B': ('Move', 'Madrid'), 'C': (
            'Support', 'A'), 'D': ('Support', 'B')}
        outcomes = {}
        resolve_conflicts(conflicts, supports, actions, outcomes)
        self.assertEqual(outcomes, {'A': '[dead]', 'B': '[dead]'})

    def test_resolve_conflicts_single_winner(self):
        conflicts = {'Paris': ['A', 'B'], 'London': ['C'], 'Dublin': ['D']}
        supports = {'A': ['C'], 'B': ['D']}
        actions = {'A': ('Move', 'Paris'), 'B': ('Move', 'Paris'),
                   'C': ('Support', 'A'), 'D': ('Support', 'B')}
        outcomes = {}

        resolve_conflicts(conflicts, supports, actions, outcomes)

        self.assertEqual(
            outcomes, {'A': '[dead]', 'B': '[dead]', 'C': 'London', 'D': 'Dublin'})

    def test_resolve_conflicts_tie(self):
        conflicts = {"Paris": ["A", "B"]}
        supports = defaultdict(list, {"A": ["C"], "B": ["D"]})
        actions = {"A": ("Move", "Paris"), "B": ("Move", "Paris")}
        outcomes = {}

        resolve_conflicts(conflicts, supports, actions, outcomes)

        self.assertEqual(outcomes, {"A": "[dead]", "B": "[dead]"})

    # ----
    # outcomes
    # ----

    def test_assign_remaining_outcomes_hold(self):
        army_locations = {'A': 'Madrid', 'B': 'Barcelona'}
        actions = {'A': ('Hold', 'Madrid'), 'B': ('Hold', 'Barcelona')}
        outcomes = {}
        assign_remaining_outcomes(army_locations, actions, outcomes)
        self.assertEqual(outcomes, {'A': 'Madrid', 'B': 'Barcelona'})

    def test_assign_remaining_outcomes_move_uncontested(self):
        army_locations = {'A': 'Madrid', 'B': 'Barcelona'}
        actions = {'A': ('Move', 'Seville'), 'B': ('Hold', 'Barcelona')}
        outcomes = {}
        assign_remaining_outcomes(army_locations, actions, outcomes)
        self.assertEqual(outcomes, {'A': 'Seville', 'B': 'Barcelona'})

    def test_assign_remaining_outcomes_move_contested(self):
        army_locations = {'A': 'Madrid', 'B': 'Barcelona'}
        actions = {'A': ('Move', 'Seville'), 'B': ('Move', 'Seville')}
        outcomes = {'C': 'Seville'}  # City already occupied by another army
        assign_remaining_outcomes(army_locations, actions, outcomes)
        self.assertEqual(outcomes['A'], '[dead]')
        self.assertEqual(outcomes['B'], '[dead]')

    def test_assign_remaining_outcomes_support_uncontested(self):
        army_locations = {'A': 'Madrid', 'B': 'Barcelona'}
        actions = {'A': ('Support', 'B'), 'B': ('Hold', 'Barcelona')}
        outcomes = {}
        assign_remaining_outcomes(army_locations, actions, outcomes)
        self.assertEqual(outcomes, {'A': 'Madrid', 'B': 'Barcelona'})

    def test_assign_remaining_outcomes_conflict_mixed(self):
        army_locations = {'A': 'Madrid', 'B': 'Barcelona', 'C': 'Valencia'}
        actions = {'A': ('Hold', 'Madrid'), 'B': (
            'Move', 'Madrid'), 'C': ('Move', 'Barcelona')}
        outcomes = {'A': 'Madrid', 'B': '[dead]', 'C': 'Barcelona'}
        assign_remaining_outcomes(army_locations, actions, outcomes)
        self.assertEqual(outcomes['B'], '[dead]')
        self.assertEqual(outcomes['C'], 'Barcelona')

    def test_assign_remaining_outcomes(self):
        army_locations = {"A": "Paris", "B": "Berlin"}
        actions = {"A": ("Hold", "Paris"), "B": ("Move", "London")}
        outcomes = {}

        assign_remaining_outcomes(army_locations, actions, outcomes)

        self.assertEqual(outcomes, {"A": "Paris", "B": "London"})

    # # ----
    # # eval
    # # ----

    def test_valid_outcomes(self):
        outcomes = {'A': 'Madrid', 'B': '[dead]'}
        validate_outcomes(outcomes)  # Should not raise an error

    def test_outcomes_not_a_dict(self):
        outcomes = ['A', 'Madrid']
        with self.assertRaises(AssertionError):
            validate_outcomes(outcomes)

    # # -----
    # # print
    # # -----

    def test_rsort_and_format_results_1(self):
        outcomes = {'A': 'Madrid', 'B': 'Barcelona'}
        result = sort_and_format_results(outcomes)
        expected = "A Madrid\nB Barcelona"
        self.assertEqual(result, expected)

    def test_sort_and_format_results_2(self):
        outcomes = {"A": "Paris", "B": "Berlin"}
        result = sort_and_format_results(outcomes)
        self.assertEqual(result, "A Paris\nB Berlin")

    def test_sort_and_format_results_3(self):
        outcomes = {'D': 'Tokyo', 'C': 'Osaka',
                    'B': 'Barcelona', 'A': 'Madrid'}
        result = sort_and_format_results(outcomes)
        expected = "A Madrid\nB Barcelona\nC Osaka\nD Tokyo"
        self.assertEqual(result, expected)

    # # -----
    # # solve
    # # -----

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO(
            "A Tokyo Hold\nB Osaka Hold\nC Seoul Hold\nD Yokohama Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Tokyo\nB Osaka\nC Seoul\nD Yokohama\n")

    def test_solve_3(self):
        r = StringIO(
            "D Taipei Support A\nB Fukuoka Move Nagoya\nA Tokyo Support B\nC Nagoya Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Tokyo\nB Nagoya\nC [dead]\nD Taipei\n")

    def test_solve_4(self):
        r = StringIO("C Tokyo Support A\nA Paris Hold\nB Osaka Move Tokyo\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Paris\nB [dead]\nC [dead]\n")

    def test_solve_5(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Tokyo Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\nD Tokyo\n")

    def test_solve_7(self):
        r = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Move Berlin\nC London Support A\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Berlin\nC London\nD Paris\n")

    def test_solve_corner_case(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
$ cat TestDiplomacy.out
............................................
----------------------------------------------------------------------
Ran 44 tests in 0.001s

OK


$ coverage report -m                   >> TestDiplomacy.out
$ cat TestDiplomacy.out
............................................
----------------------------------------------------------------------
Ran 44 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          93      0     54      3    98%   56->44, 84->79, 124->117
TestDiplomacy.py     279      0     12      0   100%
--------------------------------------------------------------
TOTAL                372      0     66      3    99%
"""
