#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve, diplomacy_output

class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = ["A Madrid Hold"]
        armies = diplomacy_read(s)
        self.assertEqual(armies['A']['location'], "Madrid")
        self.assertEqual(armies['A']['action'], "Hold")
        self.assertEqual(armies['A']['supporters'], 0)  # Initialized to 0 in diplomacy_read

    def test_read_2(self):
        s = ["B Barcelona Move Madrid"]
        armies = diplomacy_read(s)
        self.assertEqual(armies['B']['location'], "Barcelona")
        self.assertEqual(armies['B']['action'], "Move")
        self.assertEqual(armies['B']['target'], "Madrid")

    def test_read_3(self):
        s = ["C London Support B"]
        armies = diplomacy_read(s)
        self.assertEqual(armies['C']['location'], "London")
        self.assertEqual(armies['C']['action'], "Support")
        self.assertEqual(armies['C']['target'], "B")

    # ----
    # eval
    # ----

    def test_eval_1(self):
        armies = {
            "A": {"location": "Madrid", "action": "Hold", "target": None, "status": "Madrid"},
            "B": {"location": "Barcelona", "action": "Hold", "target": None, "status": "Barcelona"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result['A']['status'], "Madrid")
        self.assertEqual(result['B']['status'], "Barcelona")

    def test_eval_2(self):
        armies = {
            "A": {"location": "Austin", "action": "Support", "target": "C", "status": "Austin"},
            "B": {"location": "Barcelona", "action": "Move", "target": "Paris", "status": "Barcelona"},
            "C": {"location": "Madrid", "action": "Move", "target": "Paris", "status": "Madrid"},
            "D": {"location": "Paris", "action": "Hold", "target": None, "status": "Paris"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result['A']['status'], "Austin")
        self.assertEqual(result['B']['status'], "[dead]")
        self.assertEqual(result['C']['status'], "Paris")
        self.assertEqual(result['D']['status'], "[dead]")

    def test_eval_3(self):
        armies = {
            "A": {"location": "Madrid", "action": "Hold", "target": None, "status": "Madrid"},
            "B": {"location": "Barcelona", "action": "Support", "target": "A", "status": "Barcelona"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result['A']['status'], "Madrid")
        self.assertEqual(result['B']['status'], "Barcelona")

    def test_eval_4(self):
        armies = {
            "A": {"location": "Madrid", "action": "Move", "target": "Barcelona", "status": "Madrid"},
            "B": {"location": "Barcelona", "action": "Support", "target": "A", "status": "Barcelona"},
            "C": {"location": "Austin", "action": "Move", "target": "Barcelona", "status": "Austin"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result['A']['status'], "[dead]")
        self.assertEqual(result['B']['status'], "[dead]")

    def test_eval_5(self):
        armies = {
            "A": {"location": "Madrid", "action": "Move", "target": "Paris", "status": "Madrid"},
            "B": {"location": "Austin", "action": "Support", "target": "A", "status": "Austin"},
            "C": {"location": "Paris", "action": "Hold", "target": None, "status": "Paris"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result['A']['status'], "Paris")
        self.assertEqual(result['C']['status'], "[dead]")

    def test_eval_6(self):
        armies = {
            "A": {"location": "Austin", "action": "Move", "target": "Madrid", "status": "Austin"},
            "B": {"location": "Madrid", "action": "Hold", "target": None, "status": "Madrid"},
            "C": {"location": "Paris", "action": "Move", "target": "Madrid", "status": "Paris"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result['A']['status'], "[dead]")
        self.assertEqual(result['B']['status'], "[dead]")
        self.assertEqual(result['C']['status'], "[dead]")

    def test_eval_7(self):
        armies = {
            "A": {"location": "Madrid", "action": "Support", "target": "Z", "status": "Madrid"},
            "B": {"location": "Barcelona", "action": "Hold", "target": None, "status": "Barcelona"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result["A"]["status"], "Madrid")
        self.assertEqual(result["B"]["status"], "Barcelona")

    def test_eval_8(self):
        armies = {
            "A": {"location": "Madrid", "action": "Support", "target": "B", "status": "Madrid"},
            "B": {"location": "Barcelona", "action": "Hold", "target": None, "status": "Barcelona"},
            "C": {"location": "Austin", "action": "Support", "target": "B", "status": "Austin"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result["A"]["status"], "Madrid")
        self.assertEqual(result["B"]["status"], "Barcelona")
        self.assertEqual(result["C"]["status"], "Austin")

    def test_eval_9(self):
        armies = {
            "A": {"location": "Madrid", "action": "Support", "target": "Z", "status": "Madrid"},
            "B": {"location": "Barcelona", "action": "Hold", "target": None, "status": "Barcelona"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result["A"]["status"], "Madrid")
        self.assertEqual(result["B"]["status"], "Barcelona")

    def test_eval_10(self):
        armies = {
            "A": {"location": "Madrid", "action": "Move", "target": "Paris", "status": "Madrid"},
            "B": {"location": "London", "action": "Support", "target": "C", "status": "London"},
            "C": {"location": "Paris", "action": "Hold", "target": None, "status": "Paris"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result["A"]["status"], "[dead]")
        self.assertEqual(result["B"]["status"], "London")
        self.assertEqual(result["C"]["status"], "Paris")

    def test_eval_11(self):
        armies = {
            "A": {"location": "Madrid", "action": "Hold", "target": None, "status": "Madrid"},
            "B": {"location": "Barcelona", "action": "Move", "target": "London", "status": "Barcelona"},
            "C": {"location": "London", "action": "Support", "target": "A", "status": "London"}
        }
        result = diplomacy_eval(armies)
    
        self.assertEqual(result["A"]["status"], "Madrid")
        self.assertEqual(result["B"]["status"], "[dead]")
        
        self.assertEqual(result["C"]["status"], "[dead]")

    def test_eval12(self):
        armies = {
            "A": {"location": "Paris", "action": "Hold", "target": None, "status": "Paris"},
            "B": {"location": "London", "action": "Support", "target": "A", "status": "London"},
            "C": {"location": "Berlin", "action": "Move", "target": "London", "status": "Berlin"}
        }
        result = diplomacy_eval(armies)
        self.assertEqual(result["A"]["status"], "Paris")
        self.assertEqual(result["B"]["status"], "[dead]")
        self.assertEqual(result["C"]["status"], "[dead]") 


    # -----
    # output
    # -----

    def test_output(self):
        result = {
            "A": {"status": "[dead]"},
            "B": {"status": "Madrid"},
            "C": {"status": "London"}
        }
        w = StringIO()
        diplomacy_output(result, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London")

    def test_output_empty(self):
        result = {}
        w = StringIO()
        diplomacy_output(result, w)
        self.assertEqual(w.getvalue(), "")

    # -----
    # solve
    # -----

    def test_solve_conflict_displacement(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Support A\nC Austin Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]")

    def test_solve_simple_hold(self):
        r = StringIO("A Austin Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin")

    def test_solve_chain_support(self):
        r = StringIO("A Madrid Move Paris\nB Barcelona Support A\nC Austin Support B\nD Paris Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB Barcelona\nC Austin\nD [dead]")

# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()
