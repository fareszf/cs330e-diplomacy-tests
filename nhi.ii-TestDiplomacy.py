#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_army, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----
    #
    # def test_read(self):
    #     s = "1 10\n"
    #     i, j = diplomacy_read(s)
    #     self.assertEqual(i,  1)
    #     self.assertEqual(j, 10)
    #
    # # ----
    # # eval
    # # ----
    #
    # def test_eval_1(self):
    #     v = diplomacy_eval(1, 10)
    #     self.assertEqual(v, 1)
    #
    # def test_eval_2(self):
    #     v = diplomacy_eval(100, 200)
    #     self.assertEqual(v, 1)
    #
    # def test_eval_3(self):
    #     v = diplomacy_eval(201, 210)
    #     self.assertEqual(v, 1)
    #
    # def test_eval_4(self):
    #     v = diplomacy_eval(900, 1000)
    #     self.assertEqual(v, 1)
    #
    # # -----
    # # print
    # # -----
    #
    # def test_print(self):
    #     w = StringIO()
    #     diplomacy_print(w, 1, 10, 20)
    #     self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Austin Hold\nB Houston Move Austin\nC Dallas Move Austin\nD ElPaso Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")

    def test_solve2(self):
        # test where everyone dies because support is equal
        r = StringIO("A Shanghai Hold\nB HongKong Move Shanghai\nC Beijing Support B\nD Nanjing Move Beijing")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")

    def test_solve3(self):
        r = StringIO("A Saigon Move Dalat\nB Dalat Move Vungtau\nC Vungtau Move Saigon")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Dalat\nB Vungtau\nC Saigon")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
