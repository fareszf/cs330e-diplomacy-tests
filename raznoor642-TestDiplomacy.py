import unittest
from Diplomacy import diplomacy_solve

class TestDiplomacySolve(unittest.TestCase):

    # This test verifies the simplest scenario where there is only one army holding a city. 
    # No interactions or conflicts occur, and the army remains alive in its location.
    def test_single_army_holding(self):
        input_data = ["A Madrid Hold"]
        expected_output = ["A Madrid"]
        self.assertEqual(diplomacy_solve(input_data), expected_output)

    # This test verifies the behavior when two armies clash with equal support, 
    # resulting in both armies being destroyed.
    def test_equal_support_conflict(self):
        input_data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid"
        ]
        expected_output = [
            "A [dead]",
            "B [dead]",
            "C [dead]"
        ]
        self.assertEqual(diplomacy_solve(input_data), expected_output)

    # This test verifies the behavior when one army has more support than others 
    # in a conflict and wins the contested city.
    def test_unequal_support_conflict(self):
        input_data = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid",
            "D Paris Support B"
        ]
        expected_output = [
            "A [dead]",
            "B Madrid",
            "C [dead]",
            "D Paris"
        ]
        self.assertEqual(diplomacy_solve(input_data), expected_output)

# Run the tests
if __name__ == "__main__":
    unittest.main()
