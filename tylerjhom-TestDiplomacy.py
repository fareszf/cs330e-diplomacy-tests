from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve, read_input, write_output


class TestDiplomacy(TestCase):
    # Tests for read_input function

    def test_read_input_1(self):
        # Test read_input with a basic set of actions
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        actions = read_input(s)
        expected = [
            ['A', 'Madrid', 'Hold', None],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Support', 'B']
        ]
        self.assertEqual(actions, expected)  # Verify the parsed actions match the expected output

    def test_read_input_2(self):
        # Test read_input with multiple actions, including different action types
        s = StringIO(
            "A Austin Move Dallas\n"
            "D Dallas Hold\n"
            "M Miami Support A\n"
            "E ElPaso Move Miami\n"
            "O OU Support M\n"
            "H Houston Support E\n"
        )
        actions = read_input(s)
        expected = [
            ['A', 'Austin', 'Move', 'Dallas'],
            ['D', 'Dallas', 'Hold', None],
            ['M', 'Miami', 'Support', 'A'],
            ['E', 'ElPaso', 'Move', 'Miami'],
            ['O', 'OU', 'Support', 'M'],
            ['H', 'Houston', 'Support', 'E']
        ]
        self.assertEqual(actions, expected)  # Verify the parsed actions match the expected output

    def test_read_input_3(self):
        # Test read_input with a sample input scenario
        s = StringIO(
            "C CorpusCristi Hold\n"
            "A Austin Move CorpusCristi\n"
            "G Georgia Move CorpusCristi\n"
            "S SanAntonio Support G\n"
        )
        actions = read_input(s)
        expected = [
            ['C', 'CorpusCristi', 'Hold', None],
            ['A', 'Austin', 'Move', 'CorpusCristi'],
            ['G', 'Georgia', 'Move', 'CorpusCristi'],
            ['S', 'SanAntonio', 'Support', 'G']
        ]
        self.assertEqual(actions, expected)  # Verify the parsed actions match the expected output

    def test_read_input_empty_input(self):
        # Test read_input with empty input to ensure it returns an empty list
        s = StringIO("\n\n")
        actions = read_input(s)
        expected = []
        self.assertEqual(actions, expected)  # Verify that no actions are parsed from empty input

    def test_read_input_extra_spaces(self):
        # Test read_input with extra spaces between fields to ensure proper parsing
        s = StringIO("A  Madrid   Hold\nB Barcelona   Move   Madrid\nC    London Support    B\n")
        actions = read_input(s)
        expected = [
            ['A', 'Madrid', 'Hold', None],
            ['B', 'Barcelona', 'Move', 'Madrid'],
            ['C', 'London', 'Support', 'B']
        ]
        self.assertEqual(actions, expected)  # Verify the parsed actions handle extra spaces correctly

    # Tests for write_output function

    def test_write_output_1(self):
        # Test write_output with multiple army statuses
        army_statuses = {
            'A': '[dead]',
            'B': 'Austin',
            'D': 'Paris'
        }
        w = StringIO()
        write_output(w, army_statuses)
        expected_output = 'A [dead]\nB Austin\nD Paris\n'
        self.assertEqual(w.getvalue(), expected_output)  # Verify the output matches the expected string

    def test_write_output_2(self):
        # Test write_output with different army statuses
        army_statuses = {
            'A': 'America',
            'C': 'Canada',
            'D': 'Cuba'
        }
        w = StringIO()
        write_output(w, army_statuses)
        expected_output = 'A America\nC Canada\nD Cuba\n'
        self.assertEqual(w.getvalue(), expected_output)  # Verify the output matches the expected string

    def test_write_output_3(self):
        # Test write_output with a mix of alive and dead armies
        army_statuses = {
            'G': 'Guam',
            'H': 'Hungry',
            'I': '[dead]'
        }
        w = StringIO()
        write_output(w, army_statuses)
        expected_output = 'G Guam\nH Hungry\nI [dead]\n'
        self.assertEqual(w.getvalue(), expected_output)  # Verify the output matches the expected string

    def test_write_output_empty_output(self):
        # Test write_output with an empty army_statuses dictionary
        army_statuses = {}
        w = StringIO()
        write_output(w, army_statuses)
        expected_output = ''
        self.assertEqual(w.getvalue(), expected_output)  # Verify that no output is written for empty statuses

    def test_write_output_single_entry(self):
        # Test write_output with a single army status
        army_statuses = {'Z': 'Zurich'}
        w = StringIO()
        write_output(w, army_statuses)
        expected_output = 'Z Zurich\n'
        self.assertEqual(w.getvalue(), expected_output)  # Verify the output matches the expected string

    # Tests for diplomacy_solve function

    def test_diplomacy_solve_1(self):
        # Test diplomacy_solve with a simple scenario
        s = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Support B\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        expected_output = "A [dead]\nB Madrid\nC London\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    def test_diplomacy_solve_2(self):
        # Test diplomacy_solve with multiple supports and moves
        s = StringIO(
            "A Austin Move Dallas\n"
            "D Dallas Hold\n"
            "M Miami Support A\n"
            "E ElPaso Move Miami\n"
            "O OU Support M\n"
            "H Houston Support E\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        expected_output = "A [dead]\nD [dead]\nE [dead]\nH Houston\nM [dead]\nO OU\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    def test_diplomacy_solve_3(self):
        # Test diplomacy_solve with support and multiple moves
        s = StringIO(
            "C CorpusCristi Hold\n"
            "A Austin Move CorpusCristi\n"
            "G Georgia Move CorpusCristi\n"
            "S SanAntonio Support G\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        expected_output = "A [dead]\nC [dead]\nG CorpusCristi\nS SanAntonio\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    def test_diplomacy_solve_4(self):
        # Test diplomacy_solve with mixed actions and supports
        s = StringIO(
            "A Austin Move Dallas\n"
            "D Dallas Support H\n"
            "E ElPaso Move Houston\n"
            "H Houston Hold\n"
            "F FtWorth Support H\n"
            "P Paris Support E\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        expected_output = "A [dead]\nD [dead]\nE [dead]\nF FtWorth\nH [dead]\nP Paris\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    def test_diplomacy_solve_all_hold(self):
        # Test diplomacy_solve where all armies hold their positions
        s = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Hold\n"
            "C London Hold\n"
            "D Dallas Hold\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        expected_output = "A Madrid\nB Barcelona\nC London\nD Dallas\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    def test_diplomacy_solve_multiple_supports(self):
        # Test diplomacy_solve with multiple supports for a single move
        s = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Support B\n"
            "D Dallas Support B\n"
            "E ElPaso Support B\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        expected_output = "A [dead]\nB Madrid\nC London\nD Dallas\nE ElPaso\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    def test_diplomacy_solve_tie_support(self):
        # Test diplomacy_solve where supports result in a tie
        s = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Move Madrid\n"
            "D Dallas Support B\n"
            "E ElPaso Support C\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        expected_output = "A [dead]\nB [dead]\nC [dead]\nD Dallas\nE ElPaso\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    def test_diplomacy_solve_support_invalidated(self):
        # Test diplomacy_solve where support is invalidated by a successful attack
        s = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Support B\n"
            "D Dallas Move London\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        # Corrected expected output based on problem rules
        expected_output = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    def test_diplomacy_solve_no_supports(self):
        # Test diplomacy_solve with no support actions, only moves and holds
        s = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Move Madrid\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        expected_output = "A [dead]\nB [dead]\nC [dead]\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    def test_diplomacy_solve_complex_scenario(self):
        # Test diplomacy_solve with a complex scenario involving multiple supports and moves
        s = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Support B\n"
            "D Dallas Move London\n"
            "E ElPaso Support D\n"
            "F FtWorth Move Dallas\n"
            "G Geneva Support F\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        # Corrected expected output based on problem rules
        expected_output = "A [dead]\nB [dead]\nC [dead]\nD London\nE ElPaso\nF Dallas\nG Geneva\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify the final army statuses

    # Additional Tests for Edge Cases

    def test_diplomacy_solve_duplicate_armies(self):
        """
        Test scenario where two armies have the same identifier.
        Assuming army identifiers are unique, this should be handled appropriately.
        """
        s = StringIO(
            "A Madrid Hold\n"
            "A Barcelona Move Madrid\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        # Behavior depends on implementation; assuming last action overwrites
        expected_output = "A Madrid\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify that the last action is reflected

    def test_diplomacy_solve_invalid_actions(self):
        """
        Test scenario with invalid actions.
        Assuming the implementation skips or handles them gracefully.
        """
        s = StringIO(
            "A Madrid Fly Paris\n"  # Invalid input action: Fly
            "B Barcelona Move Madrid\n"
            "C London Support B\n"
        )
        w = StringIO()
        diplomacy_solve(s, w)
        # Since Fly is invalid, A remains in Madrid
        expected_output = "A Madrid\nB Madrid\nC London\n"
        self.assertEqual(w.getvalue(), expected_output)  # Verify that invalid actions are handled

    def test_diplomacy_solve_no_armies(self):
        """
        Test scenario with no armies.
        """
        s = StringIO("")
        w = StringIO()
        diplomacy_solve(s, w)
        expected_output = ""
        self.assertEqual(w.getvalue(), expected_output)  # Verify that no output is produced when there are no armies


if __name__ == '__main__': #pragma no cover
    main()
