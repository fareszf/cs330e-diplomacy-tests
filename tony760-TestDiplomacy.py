from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin support D")
        w = StringIO()
        diplomacy_solve(r, w)
        print(w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")
    
        
    def test_solve_2(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        print(w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    def test_solve_3(self):
        r = StringIO("A Madrid Move Paris\nB Paris Hold\nC London Support B\nD Berlin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        print(w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_4(self):
        r = StringIO("A Madrid Move Barcelona\nB Paris Move Barcelona\nC London Move Barcelona")
        w = StringIO()
        diplomacy_solve(r, w)
        print(w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve_5(self):
        r = StringIO("A Madrid Move Paris\nB Barcelona Support A\nC London Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        print(w)
        self.assertEqual(w.getvalue(), "A Paris\nB Barcelona\nC London\n")
    
# ----
# main
# ----


if __name__ == "__main__":
    main()
    
    
""" #pragma: no cover
coverage run   --source=. --branch TestDiplomacy.py > TestDiplomacy.tmp 2>&1
coverage report -m                      >> TestDiplomacy.tmp
cat TestDiplomacy.tmp
.....
----------------------------------------------------------------------
Ran 5 tests in 0.001s

OK
<_io.StringIO object at 0x1015a7ac0>
<_io.StringIO object at 0x1015a7b80>
<_io.StringIO object at 0x1015a7c40>
<_io.StringIO object at 0x1015a7d00>
<_io.StringIO object at 0x1015a7e80>
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          88      3     38      2    96%   105, 118-119
RunDiplomacy.py        4      4      2      0     0%   1-10
TestDiplomacy.py      36      0      2      1    97%   48->exit
--------------------------------------------------------------
TOTAL                128      7     42      3    93%
"""