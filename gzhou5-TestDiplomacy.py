from unittest import main, TestCase
from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase): 
    def test_single_army_holding(self): 
        # test case: one army holding a position w no conflict
        input_data = ["A Madrid Hold"]
        expected_output = "A Madrid"
        self.assertEqual(diplomacy_solve(input_data), expected_output)
    
    def test_two_armies_conflict(self): 
        # test case: two armies moving to same location, both die
        input_data = [
            "A Madrid Hold", 
            "B Barcelona Move Madrid"
            ]
        expected_output = "A [dead]\nB [dead]"
        self.assertEqual(diplomacy_solve(input_data), expected_output)
    
    def test_supported_attack_wins(self): 
        #test case: army B attacks A w support, A is holding, A should die, B should occupy
        input_data = [
            "A Madrid Hold", 
            "B Barcelona Move Madrid",
            "C London Support B"
        ]
        expected_output = "A [dead]\nB Madrid\nC London"
        self.assertEqual(diplomacy_solve(input_data), expected_output)

if __name__ == "__main__":
    main()