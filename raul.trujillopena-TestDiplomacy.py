#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve 

# -------------
# TestDiplomacy
# -------------


class TestDiplomacy(TestCase):
    # -----
    # solve
    # -----

	def test_solve_1(self):
		r = StringIO("A Austin Hold\nD Dallas Move Austin\nH Houston Support A\nG Galveston Move Austin\nL Laredo Support G\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(),"A [dead]\nD [dead]\nG [dead]\nH Houston\nL Laredo\n")

	def test_solve_2(self):
		r = StringIO("A Austin Hold\nD Dallas Move Austin\nH Houston Support A\nG Galveston Move Austin\nL Laredo Support G\nP Paris Support A\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(),"A Austin\nD [dead]\nG [dead]\nH Houston\nL Laredo\nP Paris\n")

	def test_solve_3(self):
		r = StringIO("A Austin Hold\nD Dallas Move Austin\nH Houston Move Dallas\nG Galveston Move Dallas\nL Laredo Support D\nP Paris Move Laredo\n")
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(),"A [dead]\nD [dead]\nG [dead]\nH [dead]\nL [dead]\nP [dead]\n")




# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py > TestDiplomacy.out 2>&1
$ cat TestDiplomacy.out
...
----------------------------------------------------------------------
Ran 3 tests in 0.001s

OK



$ coverage report -m >> TestDiplomacy.out
$ cat TestDiplomacy.out
...
----------------------------------------------------------------------
Ran 3 tests in 0.001s

OK
(py330e) raultrujillo-pena@Jrs-MacBook-Pro diplomacy % coverage report -m >> TestDiplomacy.out
(py330e) raultrujillo-pena@Jrs-MacBook-Pro diplomacy % cat TestDiplomacy.out
...
----------------------------------------------------------------------
Ran 3 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py         100      6     42      5    92%   17, 44, 89-90, 92, 114, 133->126, 142->145
TestDiplomacy.py      21      0      0      0   100%
--------------------------------------------------------------
TOTAL                121      6     42      5    93%
"""