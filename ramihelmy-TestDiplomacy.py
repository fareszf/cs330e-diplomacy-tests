from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_eval, diplomacy_output

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    
    # read
    def test_read1(self):
        r = StringIO("A Madrid Hold\n")
        l = diplomacy_read(r)
        self.assertEqual(l,  [["A", "Madrid", "Hold"]])
    
    def test_read2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        l = diplomacy_read(r)
        self.assertEqual(l,  [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"]])
    
    def test_read3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        l = diplomacy_read(r)
        self.assertEqual(l,  [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Support", "B"], ["D", "Austin", "Move", "London"]])

    # output
    def test_out1(self):
        statuses = {'A':"[dead]"}
        w = StringIO()
        diplomacy_output(w, statuses)
        self.assertEqual(w.getvalue(), "A [dead]\n")

    # eval
    def test_eval1(self):
        bruh = diplomacy_eval([["A", "Madrid", "Hold"]])
        self.assertEqual(bruh, {'A':"Madrid"})
    
    def test_eval2(self):
        bruh = diplomacy_eval([["A", 'Madrid', "Hold"], 
                               ["B", "Barcelona", "Move", "Madrid"], 
                               ["C", "London", "Move", "Madrid"], 
                               ['D', "Paris", "Support", "B"], 
                               ["E", 'Austin', "Support", "A"]])
        self.assertEqual(bruh, {'A':"[dead]", 'B':"[dead]", 'C':"[dead]", 'D':'Paris', 'E':'Austin'})

    def test_eval3(self):
        bruh = diplomacy_eval([["A", 'Madrid', "Hold"], 
                               ["B", "Barcelona", "Move", "Madrid"], 
                               ["C", "London", "Support", "B"],
                               ["D", "Austin", "Move", "London"]])
        self.assertEqual(bruh, {'A':"[dead]", 'B':"[dead]", 'C':"[dead]", 'D':"[dead]"})

    # solve
    def test_solve1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve2(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve3(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Hold\nC Austin Support A\nD Paris Support A\n E Budapest Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB [dead]\nC Austin\nD Paris\nE Budapest\n")
    
    def test_solve4(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Hold\nC Austin Support A\nD Paris Support A\n E Budapest Support B\nF Amsterdam Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Austin\nD Paris\nE Budapest\nF Amsterdam\n")
    
    def test_solve5(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Hold\nC Austin Support A\nD Paris Support A\n E Budapest Support B\nF Amsterdam Support B\nG Chicago Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC Austin\nD [dead]\nE Budapest\nF Amsterdam\nG [dead]\n")

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1

$ coverage report -m                   >> TestDiplomacy.out

"""
