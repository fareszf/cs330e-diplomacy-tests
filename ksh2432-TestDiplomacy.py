from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):
    # -----
    # solve
    # -----

    def test_solve_circular_support(self):
        """Test case where armies form a circular support chain."""
        r = StringIO("A Madrid Support B\nB Barcelona Support C\nC London Support A\nD Paris Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")

    def test_solve_multiple_support_cancellation(self):
        """Test case where multiple armies try to cancel support simultaneously."""
        r = StringIO(
            "A Madrid Hold\n"           # Holding army
            "B Barcelona Move Madrid\n"  # Attacker with multiple supports
            "C London Support B\n"       # Supporter 1
            "D Paris Support B\n"        # Supporter 2
            "E Austin Move London\n"     # Attacks first supporter
            "F Rome Move Paris\n"        # Attacks second supporter
        )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")

    def test_solve_support_chain_attack(self):
        """Test case with a chain of supports being attacked at different points."""
        r = StringIO(
            "A Madrid Hold\n"           # Base army being supported
            "B Barcelona Support A\n"    # First supporter
            "C London Support B\n"       # Supports the supporter
            "D Paris Support C\n"        # Supports the supporter's supporter
            "E Austin Move London\n"     # Attacks middle of support chain
            "F Rome Move Paris\n"        # Attacks end of support chain
        )
        w = StringIO()
        diplomacy_solve(r, w)
        # Support chain should be broken, making initial supports invalid
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")
        
    def test_solve_no_support(self):
        """Test case where no armies support each other."""
        r = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Move Madrid\n"
            "D Paris Move Madrid\n"
        )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_post_condition(self):
        """Test case to cover post-condition check in resolve_conflicts."""
        r = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Support B\n"
        )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
# ----
# main
# ----

if __name__ == "__main__":
    main()