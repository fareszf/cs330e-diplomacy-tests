#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    def test_no_conflict(self):

        r = StringIO('A Madrid Hold')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\n')

    def test_single_conflict(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')
        
    def test__multi_conflict(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n')

    def test_support_invalidated(self):
        r = StringIO('A Madrid Support B\nB Barcelona Move Paris\nC London Move Madrid\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Paris\nC [dead]\n')

    def test_support_valid(self):
        r = StringIO('A Madrid Support B\nB Barcelona Move Paris\nC London Move Paris\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\nB Paris\nC [dead]\n')

    def test_equal_support(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Support B\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC London\nD Paris\n')

    def test_conflicted_uncontested(self):
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move Paris\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB Madrid\nC London\nD Paris\n')

    def test_edge_case_single_army_support(self):
        r = StringIO('A Madrid Hold\nB Barcelona Support A\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\nB Barcelona\n')

    def test_invalid_action(self):
        r = StringIO('A Madrid invalidinput\n')
        w = StringIO()
        with self.assertRaises(ValueError):
            diplomacy_solve(r, w)


if __name__ == "__main__":
    main()
    
""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.........
----------------------------------------------------------------------
Ran 9 tests in 0.001s

OK
(base) evazhu@MacBook-Pro-5 diplomacy % coverage report
Name               Stmts   Miss Branch BrPart  Cover
----------------------------------------------------
Diplomacy.py          91      2     70      4    96%
TestDiplomacy.py      51      1      4      1    96%
----------------------------------------------------
TOTAL                142      3     74      5    96%
