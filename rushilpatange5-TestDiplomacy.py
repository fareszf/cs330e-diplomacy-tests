#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
import unittest
from diplomacy import diplomacy_solve

# -------------
# TestDiplomacy
# -------------

class TestDiplomacy(unittest.TestCase):

    # Test Case 1: Single army holding position without conflict.
    def test_single_hold(self):
        r = StringIO("X Tokyo Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "X Tokyo\n")

    # Test Case 2: Two armies clash without support; both should die.
    def test_clash_without_support(self):
        r = StringIO("X Berlin Hold\nY Vienna Move Berlin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "X [dead]\nY [dead]\n")

    # Test Case 3: Army wins with support, defeating the holding army.
    def test_attack_with_support(self):
        r = StringIO("X Oslo Hold\nY Stockholm Move Oslo\nZ Helsinki Support Y\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "X [dead]\nY Oslo\nZ Helsinki\n")

# ----
# main
# ----

if __name__ == "__main__":
    unittest.main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py > TestDiplomacy.out 2>&1
$ coverage report -m >> TestDiplomacy.out
"""
