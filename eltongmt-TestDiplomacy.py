#!/usr/bin/env python3

# ----
# imports
# ----

from Diplomacy import diplomacy_solve
from io import StringIO
from unittest import main, TestCase

# ----
# test
# ----

class TestDiplomacy (TestCase):
                  
    # ----
    # read
    # ----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona\nD Paris Support B\nE Austin Support A")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
                w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\nD Paris\nE Austin\n")


    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Paris Move London")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
                w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\n")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
                w.getvalue(), "A Madrid\nB [dead]\nC London\n")

if __name__ == "__main__": # pragma: no cover 
    main()
