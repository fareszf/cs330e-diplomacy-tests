#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_print

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # solve
    # ----

    def test_solve(self):
        r = StringIO("A Madrid Hold\n B Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_support(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
    def test_solve_same_support(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD Paris\n")
        
# ----
# main
# ----


if __name__ == "__main__":
    main()
