#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------
from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_write

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Moscow Hold\nB Chennai Move Moscow\nC Nashville Support B\nD Austin Move Nashville")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Moscow\nC [dead]\nD [dead]")

    def test_solve2(self):
        r = StringIO("A Delhi Support B\nB Newark Hold\nC Austin Support B\nD NewYork Move Delhi")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Newark\nC Austin\nD [dead]")


    def test_solve3(self):
        r = StringIO("A Tallahasee Hold\nB Boston Hold\nC HongKong Support D\nD Austin Move Boston\nE Orlando Support D\nF Dallas Move Tallahasee")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC HongKong\nD Boston\nE Orlando\nF [dead]")

    def test_solve4(self):
        r = StringIO("A SanAntonio Hold\nB Lubbock Support A\nC Austin Move SanAntonio\nD Chennai Move Nashville\nE Singapore Support C\nF Tyler Move Singapore")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Lubbock\nC [dead]\nD Nashville\nE [dead]\nF [dead]")

# ----
# main
# ----



if __name__ == "__main__":
    main()


""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
"""
