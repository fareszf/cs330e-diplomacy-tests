#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, diplomacy_eval_setup, diplomacy_eval_helper

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = StringIO("A Madrid Hold")
        a = diplomacy_read(s)
        self.assertEqual(a, [['A', 'Madrid', 'Hold']])

    def test_read_2(self):
        s = StringIO("A Madrid Hold\nB London Support A\nC Barcelona Move Madrid")
        a = diplomacy_read(s)
        self.assertEqual(a, [['A', 'Madrid', 'Hold'], ['B', 'London', 'Support', 'A'], ['C', 'Barcelona', 'Move', 'Madrid']])

    def test_read_3(self):
        s = StringIO("B London Support A\nC Barcelona Move Madrid\nA Madrid Hold")
        a = diplomacy_read(s)
        self.assertEqual(a, [['A', 'Madrid', 'Hold'], ['B', 'London', 'Support', 'A'], ['C', 'Barcelona', 'Move', 'Madrid']])
        
    def test_read_4(self):
        s = StringIO("")
        a = diplomacy_read(s)
        self.assertEqual(a, [])


    # -----
    # eval_setup 
    # -----
    
    def test_eval_setup_1(self):
        a = [['A', 'Madrid', 'Hold']]
        x, y, z = diplomacy_eval_setup(a)
        self.assertEqual(x, ['A'])
        self.assertEqual(y, ['Madrid'])
        self.assertEqual(z, [0])

    def test_eval_setup_2(self):
        a = [['A', 'Madrid', 'Hold'], ['B', 'London', 'Support', 'A'], ['C', 'Barcelona', 'Move', 'Madrid']]
        x, y, z = diplomacy_eval_setup(a)
        self.assertEqual(x, ['A', 'B', 'C'])
        self.assertEqual(y, ['Madrid', 'London', 'Madrid'])
        self.assertEqual(z, [1,0,0])

    def test_eval_setup_3(self):
        a = [['A', 'Madrid', 'Hold'], ['B', 'London', 'Move', 'Madrid'], ['C', 'Barcelona', 'Move', 'Madrid']]
        x, y, z = diplomacy_eval_setup(a)
        self.assertEqual(x, ['A', 'B', 'C'])
        self.assertEqual(y, ['Madrid', 'Madrid', 'Madrid'])
        self.assertEqual(z, [0,0,0])
        
    def test_eval_setup_4(self):
        a = [['A', 'Madrid', 'Hold'], ['B', 'London', 'Support', 'A'], ['C', 'Barcelona', 'Support', 'B']]
        x, y, z = diplomacy_eval_setup(a)
        self.assertEqual(x, ['A', 'B', 'C'])
        self.assertEqual(y, ['Madrid', 'London', 'Barcelona'])
        self.assertEqual(z, [1,1,0])
        
    def test_eval_setup_5(self):
        a = [['A', 'Madrid', 'Support', 'C'], ['B', 'London', 'Support', 'A'], ['C', 'Barcelona', 'Support', 'B']]
        x, y, z = diplomacy_eval_setup(a)
        self.assertEqual(x, ['A', 'B', 'C'])
        self.assertEqual(y, ['Madrid', 'London', 'Barcelona'])
        self.assertEqual(z, [1,1,1])
        
    def test_eval_setup_6(self):
        a = []
        x, y, z = diplomacy_eval_setup(a)
        self.assertEqual(x, [])
        self.assertEqual(y, [])
        self.assertEqual(z, [])
        

    # -----
    # eval_helper 
    # -----
    
    def test_eval_helper_1(self):
        x = ['A']
        y = ['Madrid']
        z = [0]
        v = diplomacy_eval_helper(x, y, z)
        self.assertEqual(v, [['A', 'Madrid']])

    def test_eval_helper_2(self):
        x = ['A', 'B', 'C']
        y = ['Madrid', 'London', 'Madrid']
        z = [1,0,0]
        v = diplomacy_eval_helper(x, y, z)
        self.assertEqual(v, [['A', 'Madrid'],['B', 'London'],['C', '[dead]']])

    def test_eval_helper_3(self):
        x = ['A', 'B', 'C']
        y = ['Madrid', 'Madrid', 'Madrid']
        z = [0,0,0]
        v = diplomacy_eval_helper(x,y,z)
        self.assertEqual(v, [['A', '[dead]'],['B', '[dead]'],['C', '[dead]']])

    def test_eval_helper_4(self):
        x = ['A', 'B', 'C', 'D']
        y = ['Madrid', 'Madrid', 'London', 'London']
        z = [0, 1, 0, 0]
        v = diplomacy_eval_helper(x,y,z)
        self.assertEqual(v, [['A', '[dead]'],['B', 'Madrid'],['C', '[dead]'], ['D', '[dead]']])
        
    def test_eval_helper_5(self):
        x = ['A', 'B', 'C', 'D']
        y = ['Madrid', 'Madrid', 'London', 'London']
        z = [0, 0, 0, 0]
        v = diplomacy_eval_helper(x,y,z)
        self.assertEqual(v, [['A', '[dead]'],['B', '[dead]'],['C', '[dead]'], ['D', '[dead]']])


    # ----
    # eval
    # ----

    def test_eval_1(self):
        a = [['A', 'Madrid', 'Hold']]
        v = diplomacy_eval(a)
        self.assertEqual(v, [['A', 'Madrid']])

    def test_eval_2(self):
        a = [['A', 'Madrid', 'Hold'], ['B', 'London', 'Support', 'A'], ['C', 'Barcelona', 'Move', 'Madrid']]
        v = diplomacy_eval(a)
        self.assertEqual(v, [['A', 'Madrid'],['B', 'London'],['C', '[dead]']])

    def test_eval_3(self):
        a = [['A', 'Madrid', 'Hold'], ['B', 'London', 'Move', 'Madrid'], ['C', 'Barcelona', 'Move', 'Madrid']]
        v = diplomacy_eval(a)
        self.assertEqual(v, [['A', '[dead]'],['B', '[dead]'],['C', '[dead]']])

    def test_eval_4(self):
        a = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B'], ['D', 'Austin', 'Move', 'London']]
        v = diplomacy_eval(a)
        self.assertEqual(v, [['A', '[dead]'],['B', '[dead]'],['C', '[dead]'], ['D', '[dead]']])

    def test_eval_5(self):
        a = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Move', 'Madrid'], ['D', 'Paris', 'Support', 'B']]
        v = diplomacy_eval(a)
        self.assertEqual(v, [['A', '[dead]'],['B', 'Madrid'],['C', '[dead]'], ['D', 'Paris']])

    def test_eval_6(self):
        a = [['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move', 'Madrid'],['C', 'London', 'Move', 'Madrid'],['D', 'Paris', 'Support','B'],['E', 'Austin','Support','A']]
        v = diplomacy_eval(a)
        self.assertEqual(v, [['A', '[dead]'],['B', '[dead]'],['C', '[dead]'], ['D', 'Paris'], ['E', 'Austin']])


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [['A', 'Madrid']])
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [['A', 'Madrid'],['B', 'London']])
        self.assertEqual(w.getvalue(), "A Madrid\nB London\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [['A', 'Madrid'],['B', '[dead]'],['C', 'London']])
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC London\n")


    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB London Support A\nC Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("B London Support A\nC Barcelona Move Madrid\nA Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC [dead]\n")

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()


