#!/usr/bin/env python3

# -------
# imports
# -------

from unittest import main, TestCase
from Diplomacy import diplomacy_read, get_army_dict, update_given_support,calculate_support,get_winner, diplomacy_eval,diplomacy_print, diplomacy_solve
from io import StringIO


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # def __init__(self):
    #     self.army_dict1 = {'A':{'desired_pos': 'Madrid','move':'Support', 'supported_army': 'C', 'support':0}, \
    #                  'B':{'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
    #                     'C': {'desired_pos': 'Austin','move':'Move', 'supported_army': None, 'support':0}}
        
    #     self.army_dict2 = {'A':{'desired_pos': 'Madrid','move':'Support', 'supported_army': 'C', 'support':0}, \
    #                  'B':{'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
    #                     'C': {'desired_pos': 'Austin','move':'Support', 'supported_army': 'A', 'support':0}}

    # -----
    # diplomacy_read
    # -----
    def test_read(self):
        r = StringIO("A Madrid Hold\nB London Support A\n")
        all_armies = diplomacy_read(r)
        self.assertEqual(
            all_armies, [['A', 'Madrid', 'Hold'], ['B', 'London', 'Support', 'A']])
        
    def test_read1(self):
        r = StringIO("A Madrid Hold\nB London Support A\nC Austin Move London\n")
        all_armies = diplomacy_read(r)
        self.assertEqual(
            all_armies, [['A', 'Madrid', 'Hold'], ['B', 'London', 'Support', 'A'], ['C', 'Austin', 'Move', 'London']])


    # -----
    # army_dict
    # -----
            
    def test_army_dict1(self):
        army_list = [['A', 'Madrid', 'Hold'], ['B', 'London', 'Move', 'Madrid']]
        
        armies = get_army_dict(army_list)

        self.assertEqual(armies['A'], {'desired_pos': 'Madrid','move':'Hold', 'supported_army': None, 'support':0})
        self.assertEqual(armies['B'], {'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0})
    
    def test_army_dict2(self):
        army_list = [['A', 'Madrid', 'Hold'],\
                      ['B', 'London', 'Move', 'Madrid'],\
                       ['C', 'York', 'Hold'],\
                         ['D', 'Cork', 'Move', 'York']]
        
        armies = get_army_dict(army_list)

        dictionary = {'A': {'desired_pos': 'Madrid','move':'Hold', 'supported_army': None, 'support':0},\
                      'B': {'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
                        'C': {'desired_pos': 'York','move':'Hold', 'supported_army': None, 'support':0},\
                        'D': {'desired_pos': 'York','move':'Move', 'supported_army': None, 'support':0}}
                      

        self.assertEqual(armies, dictionary)

    def test_army_dict3(self):
        army_list = [['A', 'Madrid', 'Hold'],\
                      ['B', 'London', 'Move', 'Madrid'],\
                       ['C', 'York', 'Support', 'A'],\
                         ['D', 'Cork', 'Move', 'York']]
        
        armies = get_army_dict(army_list)

        dictionary = {'A': {'desired_pos': 'Madrid','move':'Hold', 'supported_army': None, 'support':0},\
                      'B': {'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
                        'C': {'desired_pos': 'York','move':'Support', 'supported_army': 'A', 'support':0},\
                        'D': {'desired_pos': 'York','move':'Move', 'supported_army': None, 'support':0}}
                      

        self.assertEqual(armies, dictionary)



    # -----
    # update_given_support
    # -----

    def test_update(self):
        army_dict = {'A':{'desired_pos': 'Madrid','move':'Support', 'supported_army': 'C', 'support':0}, \
                     'B':{'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
                        'C': {'desired_pos': 'Austin','move':'Move', 'supported_army': None, 'support':0}}
        
        armies, city_dict = update_given_support(army_dict)

        self.assertEqual(armies['A'], {'desired_pos': 'Madrid','move':'Support', 'supported_army': None, 'support':0})
        self.assertEqual(armies['B'], {'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0})
        self.assertEqual(armies['C'], {'desired_pos': 'Austin','move':'Move', 'supported_army': None, 'support':0})

    def test_update1(self):
        army_dict = {'A':{'desired_pos': 'Madrid','move':'Support', 'supported_army': 'C', 'support':0}, \
                     'B':{'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
                        'C': {'desired_pos': 'Austin','move':'Support', 'supported_army': 'A', 'support':0}}
        
        armies, city_dict = update_given_support(army_dict)

        self.assertEqual(armies['A'], {'desired_pos': 'Madrid','move':'Support', 'supported_army': None, 'support':0})
        self.assertEqual(armies['B'], {'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0})
        self.assertEqual(armies['C'], {'desired_pos': 'Austin','move':'Support', 'supported_army': 'A', 'support':0})

    # -----
    # test city_dictionary
    # -----

    def test_city_dict(self):
        army_dict = {'A':{'desired_pos': 'Madrid','move':'Support', 'supported_army': 'D', 'support':0}, \
                     'B':{'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
                        'C': {'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
                        'D': {'desired_pos': 'London','move':'Support', 'supported_army': 'B', 'support':0}}
        
        armies, city_dict = update_given_support(army_dict)

        self.assertEqual(city_dict['Madrid'], ['A', 'B', 'C'])
        self.assertEqual(city_dict['London'], ['D'])
    
    def test_city_dict1(self):
        army_dict = {'A':{'desired_pos': 'Austin','move':'Support', 'supported_army': 'D', 'support':0}, \
                     'B':{'desired_pos': 'Austin','move':'Move', 'supported_army': None, 'support':0},\
                        'C': {'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
                        'D': {'desired_pos': 'London','move':'Support', 'supported_army': 'B', 'support':0}}
        
        armies, city_dict = update_given_support(army_dict)

        self.assertEqual(city_dict['Austin'], ['A', 'B'])
        self.assertEqual(city_dict['Madrid'], ['C'])
        self.assertEqual(city_dict['London'], ['D'])

    # -----
    # calculate_support
    # -----

    def test_calculate(self):
        army_dict = {'A':{'desired_pos': 'Madrid','move':'Support', 'supported_army': 'C', 'support':0}, \
                     'B':{'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0},\
                        'C': {'desired_pos': 'Austin','move':'Support', 'supported_army': 'B', 'support':0}, \
                        'D': {'desired_pos': 'SanAntonio','move':'Support', 'supported_army': 'B', 'support':0}, \
                        'E': {'desired_pos': 'NewYork','move':'Support', 'supported_army': 'A', 'support':0}}
        
        armies = calculate_support(army_dict)

        self.assertEqual(armies['A'], {'desired_pos': 'Madrid','move':'Support', 'supported_army': 'C', 'support':1})
        self.assertEqual(armies['B'], {'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':2})
        self.assertEqual(armies['C'], {'desired_pos': 'Austin','move':'Support', 'supported_army': 'B', 'support':1})
        self.assertEqual(armies['D'], {'desired_pos': 'SanAntonio','move':'Support', 'supported_army': 'B', 'support':0})
        self.assertEqual(armies['E'], {'desired_pos': 'NewYork','move':'Support', 'supported_army': 'A', 'support':0})
    
    def test_calculate2(self):
        army_dict = {'A':{'desired_pos': 'Rohan', 'move':'Support', 'supported_army': 'C', 'support':0}, \
                     'B':{'desiered_pos': 'Gondor', 'move':'Move', 'supported_army': None, 'support':0}, \
                        'C':{'desired_pos': 'Gondor', 'move':'Hold', 'supported_army':None, 'support':0},\
                            'D':{'desired_pos': 'Mordor', 'move':'Support', 'supported_army': 'B', 'support':0},\
                                'E':{'desired_pos': 'Mirkwood', 'move':'Support', 'supported_army':'C', 'support':0}}
        
        armies = calculate_support(army_dict)

        self.assertEqual(armies['A'], {'desired_pos': 'Rohan', 'move':'Support', 'supported_army': 'C', 'support':0})
        self.assertEqual(armies['B'], {'desiered_pos': 'Gondor', 'move':'Move', 'supported_army': None, 'support':1})
        self.assertEqual(armies['C'], {'desired_pos': 'Gondor', 'move':'Hold', 'supported_army':None, 'support':2})
        self.assertEqual(armies['D'], {'desired_pos': 'Mordor', 'move':'Support', 'supported_army': 'B', 'support':0})
        self.assertEqual(armies['E'], {'desired_pos': 'Mirkwood', 'move':'Support', 'supported_army':'C', 'support':0})


    # -----
    # get_winner
    # -----

    def test_get_winner(self):
        army_dict = {'A':{'desired_pos': 'Madrid','move':'Hold', 'supported_army': 'C', 'support':1}, \
                     'B':{'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0}}
        armies_in_city = ['A', 'B']
        winner = get_winner(army_dict, armies_in_city)

        self.assertEqual(winner, 'A')

    def test_get_winner1(self):
        army_dict = {'A':{'desired_pos': 'Madrid','move':'Hold', 'supported_army': 'C', 'support':0}, \
                     'B':{'desired_pos': 'London','move':'Move', 'supported_army': None, 'support':0}}
        armies_in_city = ['A']
        winner = get_winner(army_dict, armies_in_city)

        self.assertEqual(winner, 'A')

    def test_get_winner2(self):
        army_dict = {'A':{'desired_pos': 'Madrid','move':'Hold', 'supported_army': 'C', 'support':0}, \
                     'B':{'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0}, \
                    'C':{'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':0}}
        armies_in_city = ['A', 'B', 'C']
        winner = get_winner(army_dict, armies_in_city)

        self.assertIsNone(winner)


    # -----
    # diplomacy_eval
    # -----

        
    def test_diplomacy_eval1(self):
        army_dict = [['A', 'Madrid','Hold'], \
                    ['B','Austin','Move', 'Madrid'],\
                    ['C', 'London','Support', 'B']]
        
        armies = diplomacy_eval(army_dict)

        self.assertEqual(armies['A'], {'desired_pos': '[dead]','move':'Hold', 'supported_army': None, 'support':0})
        self.assertEqual(armies['B'], {'desired_pos': 'Madrid','move':'Move', 'supported_army': None, 'support':1})
        self.assertEqual(armies['C'], {'desired_pos': 'London','move':'Support', 'supported_army': 'B', 'support':0})


    def test_diplomacy_eval2(self):
        army_dict = [['A', 'Madrid','Hold'], \
                    ['B','Austin','Hold'],\
                    ['C', 'London','Support', 'B']]
        
        armies = diplomacy_eval(army_dict)

        self.assertEqual(armies['A'], {'desired_pos': 'Madrid','move':'Hold', 'supported_army': None, 'support':0})
        self.assertEqual(armies['B'], {'desired_pos': 'Austin','move':'Hold', 'supported_army': None, 'support':1})
        self.assertEqual(armies['C'], {'desired_pos': 'London','move':'Support', 'supported_army': 'B', 'support':0})
    
    def test_diplomacy_eval3(self):
        army_dict = [['A', 'Madrid','Move', 'London'], \
                    ['B','Austin','Move', 'Derry'],\
                    ['C', 'London','Move', 'SanAntonio']]
        
        armies = diplomacy_eval(army_dict)

        self.assertEqual(armies['A'], {'desired_pos': 'London','move':'Move', 'supported_army': None, 'support':0})
        self.assertEqual(armies['B'], {'desired_pos': 'Derry','move':'Move', 'supported_army': None, 'support':0})
        self.assertEqual(armies['C'], {'desired_pos': 'SanAntonio','move':'Move', 'supported_army': None, 'support':0})


    # -----
    # diplomacy_solve
    # -----


    def test_solve(self):
        r = StringIO("A Madrid Hold\nB London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\n")

    def test_solve1(self):
        r = StringIO("A Barcelona Hold\nB Austin Hold\nC NewYork Hold\nD Charlotte Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Austin\nC NewYork\nD Charlotte\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve3(self):
        r = StringIO("A Madrid Move London\nB Barcelona Move London\nC London Move Madrid\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Madrid\nD [dead]\n")
    
    def test_solve4(self):
        r = StringIO("A Madrid Move London\nB London Hold\nC Austin Move London\nD Houston Support A\nE Waco Support A\nF Edinburgh Support C\nG Vancouver Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Houston\nE Waco\nF Edinburgh\nG Vancouver\n")
    
    def test_solve5(self):
        r = StringIO("A Madrid Move London\nB London Hold\nC Austin Move London\nD Houston Support A\nE Waco Support A\nF Edinburgh Support C\nG Vancouver Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB [dead]\nC [dead]\nD Houston\nE Waco\nF Edinburgh\nG Vancouver\n")
        
    def test_solve6(self):
        r = StringIO("A Madrid Move London\nB London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Madrid\n")
        
    def test_solve7(self):
        r = StringIO("A Madrid Move London\nB London Move Madrid\nC Derry Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\n")





# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()
