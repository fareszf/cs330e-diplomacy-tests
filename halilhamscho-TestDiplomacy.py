"""
3 Unit Tests for the function diplomacy_solve(), run coverage
"""
from io import StringIO
from Diplomacy import diplomacy_solve
from unittest import main, TestCase

class DiplomacyUnitTests (TestCase):
    def test_1(self):
        """
        Testing Number of Supports Being Equal
        """
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n" )

    def test_2(self):
        """
        Testing Invalidating Support
        """
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_3(self):
        """
        Testing Corner Case: An army can support another army that is already supporting
        """
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 3 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 3 tests in 0.000s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          96      8     68      9    90%   56->49, 79, 81->77, 89, 98->94, 105, 113-114, 130, 137, 142
TestDiplomacy.py      21      0      0      0   100%
--------------------------------------------------------------
TOTAL                117      8     68      9    91%
"""
