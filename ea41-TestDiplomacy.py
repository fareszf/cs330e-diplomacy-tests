# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve1(self):  # Professor's Example
        r = StringIO(
            "E Austin Support A\nD Paris Support B\nC London Move Madrid\nB Barcelona Move Madrid\nA Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve2(self):  # Texas vs Mexico
        r = StringIO(
            "A Austin Hold\nB MexicoCity Move Austin\nC WashingtonDC Support A\nD London Support A\nE Madrid Support B")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC WashingtonDC\nD London\nE Madrid\n")

    def test_solve3(self):  # World War 3
        r = StringIO(
            "A Beijing Hold\nB Taipei Move Beijing\nC Seoul Support B\nD Pyongyang Move Seoul")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve4(self):  # Single Army Hold
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve5(self):  # B supported attack on A
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve6(self):  # A and B both die attacking each other
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve7(self):  # C's support invalidated by attack from D
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve8(self):  # A is attacked by B and C
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve9(self):  # B attacks with support from D
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve10(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B \nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

# ----
# main
# ----


if __name__ == "__main__":  # pragma: no cover
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
..........
----------------------------------------------------------------------
Ran 10 tests in 0.002s

OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
..........
----------------------------------------------------------------------
Ran 10 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          53      0     30      0   100%
TestDiplomacy.py      54      0      0      0   100%
--------------------------------------------------------------
TOTAL                107      0     30      0   100%
"""
