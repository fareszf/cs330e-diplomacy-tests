#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_eval, diplomacy_print

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve_no_conflict(self):
        r = StringIO("A Madrid Hold\nB Paris Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Paris\n")

    def test_solve_simple_move(self):
        r = StringIO("A Madrid Move Barcelona\nB Paris Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Paris\n")

    def test_solve_conflict_with_support(self):
        input_data = (
            "A Madrid Move Paris\n"
            "B Paris Hold\n"
            "C Berlin Support A\n"
        )
        r = StringIO(input_data)
        w = StringIO()
        diplomacy_solve(r, w)
        expected_output = "A Paris\nB [dead]\nC Berlin\n"
        self.assertEqual(w.getvalue(), expected_output)
    
    def test_solve_support_cut(self):
        input_data = (
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Support B\n"
            "D Austin Move London\n"
        )
        r = StringIO(input_data)
        w = StringIO()
        diplomacy_solve(r, w)
        expected_output = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        self.assertEqual(w.getvalue(), expected_output)
    
    def test_solve_multiple_attackers_same_location(self):
        input_data = (
            "A Madrid Move Paris\n"
            "B Barcelona Move Paris\n"
        )
        r = StringIO(input_data)
        w = StringIO()
        diplomacy_solve(r, w)
        expected_output = "A [dead]\nB [dead]\n"
        self.assertEqual(w.getvalue(), expected_output)

# main
# ----


if __name__ == "__main__":
    main() #pragma: no cover

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
....
----------------------------------------------------------------------
Ran 4 tests in 0.001s

OK


$ coverage report -m                   >> TestDiplomacy.out

$ cat TestDiplomacy.out
....
----------------------------------------------------------------------
Ran 4 tests in 0.001s
OK

Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          70      0     40      0   100%
TestDiplomacy.py      29      0      0      0   100%
--------------------------------------------------------------
TOTAL                 99      0     40      0   100%
"""
