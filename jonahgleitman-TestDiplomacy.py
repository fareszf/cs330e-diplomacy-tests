#!/usr/bin/env python3

# TestDiplomacy.py

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy(TestCase):
   def test_read(self):
       s = "A Paris Hold\n"
       army, city, action = diplomacy_read(s)
       self.assertEqual(army, "A")
       self.assertEqual(city, "Paris")
       self.assertEqual(action, "Hold")

   def test_eval_1(self):
       """
       Test deep support invalidation chain
       A Paris Hold
       B Madrid Support A
       C London Support B
       D Berlin Move London
       F Rome Support A
       E Vienna Support D
       """
       moves = [
           ("A", "Paris", "Hold"),
           ("B", "Madrid", "Support A"),
           ("C", "London", "Support B"),
           ("D", "Berlin", "Move London"),
           ("F", "Rome", "Support A"),
           ("E", "Vienna", "Support D")
       ]
       expected = [
           "A Paris",
           "B Madrid",
           "C [dead]",
           "D [dead]",
           "E Vienna",
           "F Rome"
       ]
       self.assertEqual(diplomacy_eval(moves), expected)

   def test_eval_2(self):
       """
       Test circular movement chain
       A Paris Move London
       B London Move Berlin
       C Berlin Move Paris
       D Madrid Support A
       E Rome Support C
       """
       moves = [
           ("A", "Paris", "Move London"),
           ("B", "London", "Move Berlin"),
           ("C", "Berlin", "Move Paris"),
           ("D", "Madrid", "Support A"),
           ("E", "Rome", "Support C")
       ]
       expected = [
           "A [dead]",
           "B [dead]",
           "C Paris",
           "D Madrid",
           "E Rome"
       ]
       self.assertEqual(diplomacy_eval(moves), expected)

   def test_eval_3(self):
       """
       Test support cut-off
       A Paris Hold
       B Madrid Move Paris
       C London Support B
       D Berlin Move London
       E Rome Move London
       F Vienna Support A
       """
       moves = [
           ("A", "Paris", "Hold"),
           ("B", "Madrid", "Move Paris"),
           ("C", "London", "Support B"),
           ("D", "Berlin", "Move London"),
           ("E", "Rome", "Move London"),
           ("F", "Vienna", "Support A")
       ]
       expected = [
           "A Paris",
           "B [dead]",
           "C London",
           "D [dead]",
           "E [dead]",
           "F Vienna"
       ]
       self.assertEqual(diplomacy_eval(moves), expected)

   def test_print(self):
       w = StringIO()
       diplomacy_print(w, ["A Paris", "B [dead]"])
       self.assertEqual(w.getvalue(), "A Paris\nB [dead]\n")

   def test_solve(self):
       """
       Test complex web of support
       """
       r = StringIO("A Paris Hold\nB Madrid Move Paris\nC London Support B\nD Berlin Support A\nE Rome Move London\nF Vienna Support B\nG Munich Support A\n")
       w = StringIO()
       diplomacy_solve(r, w)
       self.assertEqual(w.getvalue(), "A Paris\nB [dead]\nC [dead]\nD Berlin\nE [dead]\nF Vienna\nG Munich\n")

if __name__ == "__main__":
   main()