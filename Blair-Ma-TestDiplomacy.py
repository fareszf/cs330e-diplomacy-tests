#!/usr/bin/env python3
# WSGI name, so pylint: disable=invalid-name
"""
Associate Files: TestDiplomacy.py, RunDiplomacy.py, RunDiplomacy.in, RunDiplomacy.out, etc...
CS 330E
Project 2
TestDiplomacy.py
The unit testing script for Diplomacy.py

Name 1: Blair Ma
Name 2: Akash Ganesh
"""

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------

# pylint: disable=line-too-long
# pylint: disable=too-many-public-methods
# pylint: disable=missing-function-docstring
class TestDiplomacy (TestCase):
    """
    Unit tests testing the diplomacy_solve(r,w) in Diplomacy.py
    """

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support A\nE Istanbul Move Madrid\nF Paris Move Madrid")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC London\nD Austin\nE [dead]\nF [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


# ----
# main
# ----


if __name__ == "__main__":
    main()

# pylint: disable=pointless-string-statement
""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestDiplomacy.out
.....
----------------------------------------------------------------------
Ran 5 tests in 0.001s

OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.....
----------------------------------------------------------------------
Ran 5 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          57      0     36      0   100%
TestDiplomacy.py      31      0      0      0   100%
--------------------------------------------------------------
TOTAL                 88      0     36      0   100%
"""
