from io import StringIO
from Diplomacy import diplomacy_solve
from unittest import main, TestCase

class DiplomacyTests (TestCase):
	def test_1 (self):
		r = StringIO('A Madrid Hold\nB Barcelona Move Madrid')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\n')

	def test_2 (self):
		r = StringIO('')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), '')

	def test_3 (self):
		r = StringIO('A Madrid Hold\nB Barcelona Support A\nC Paris Move Madrid\nD Austin Support C')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), 'A [dead]\nB Barcelona\nC [dead]\nD Austin\n')	

	def test_4 (self):
		r = StringIO('A SanAntonio Move Detroit\nB NewYork Move Detroit\nC Springfield Support A\nD Cincinatti Move Springfield')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

	def test_5 (self):
		r = StringIO('A Madrid Move Philadelphia\nB Pittsburgh Support A\nC Richmond Move Philadelphia\nD AtlanticCity Support A')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(w.getvalue(), 'A Philadelphia\nB Pittsburgh\nC [dead]\nD AtlanticCity\n')

#the comment next to  main is to tell coverege not to consider this statement
if __name__ == "__main__": #pragma: no cover
	main()