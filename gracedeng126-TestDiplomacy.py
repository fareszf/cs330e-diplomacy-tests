#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        i, j = diplomacy_read(s)
        self.assertEqual(i,  {"Madrid":['A', 'B'], "London": ['C']})
        self.assertEqual(j, {'A': [], 'B': ['C'], 'C': []})

    # ----   
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval({"Madrid":['A', 'B'], "London": ['C']}, {'A': [], 'B': ['C'], 'C': []})
        self.assertEqual(v, {"A": "[dead]", "B": "Madrid", "C": "London"})


    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        diplomacy_print({"A": "[dead]", "B": "Madrid", "C": "London"}, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move London\nC London Move Barcelona\nD Paris Support B\nE Austin Support A")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC Barcelona\nD Paris\nE Austin\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
