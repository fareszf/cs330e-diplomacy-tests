from unittest import main, TestCase
from Diplomacy import diplomacy_solve
from io import StringIO

class TestDiplomacy(TestCase):
    def test_mixed_actions_with_support_invalidation(self):
        # Basic scenario with a mix of holds, moves, and supports
        r = StringIO("A London Hold\nB Paris Move London\nC Berlin Support B\nD Rome Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        # Expected outcome: A is dead (attacked), B takes London, C and D hold positions
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC Berlin\nD Rome\n")

    def test_tied_support_conflict(self):
        # Scenario with two armies moving to the same location with equal support, resulting in a tie
        r = StringIO("A Madrid Move Paris\nB Barcelona Move Paris\nC Berlin Support A\nD Rome Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        # Expected outcome: Both A and B die due to conflict at Paris; C and D hold their original locations
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Berlin\nD Rome\n")

    def test_uncontested_holds_and_moves_with_support(self):
        # Scenario: No conflict, one move, and support to test base behavior with support invalidation
        # Expected Outcome: A is dead as B moves to A’s location without valid support, C is protected by E and wins over D.
        r = StringIO("A NewYork Hold\nB Seattle Move NewYork\nC Chicago Support B\nD Dallas Move Chicago\nE Tampa Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        # Updated expectation: A and B should both be dead, C holds Chicago with support, and D is dead
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Chicago\nD [dead]\nE Tampa\n")

    # Additional edge cases to maximize coverage:
    def test_lone_hold_no_opposition(self):
        # Scenario: Lone army holding with no other opponents or supports
        r = StringIO("A Lisbon Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        # Expected outcome: A holds Lisbon with no issues
        self.assertEqual(w.getvalue(), "A Lisbon\n")

    def test_chain_support_invalidations(self):
        # Scenario: Complex chain of support invalidations
        r = StringIO("A Houston Move Dallas\nB Seattle Move Dallas\nC ElPaso Support A\nD Dallas Move ElPaso\nE SF Support C\nF Florida Move SF")
        w = StringIO()
        diplomacy_solve(r, w)
        # Expected outcome: All armies involved in the chain are dead due to invalidated supports and conflicts
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")

    def test_mutual_support_tie(self):
        # Scenario: Mutual support leading to a tie where all units die
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        # Expected outcome: All armies moving to Madrid die due to a mutual support tie; D and E remain
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

if __name__ == "__main__":
    main()


"""
.
----------------------------------------------------------------------
Ran 3 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          95     12     50      8    85%   18, 39->26, 56, 66->71, 72-73, 97-99, 132-134, 139-140
TestDiplomacy.py      22      1      2      1    92%   32
--------------------------------------------------------------
TOTAL                117     13     52      9    86%
"""

"""
I added more to maximize coverage
.
----------------------------------------------------------------------
Ran 6 tests in 0.003s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          95      2     50      5    95%   18, 39->26, 56, 66->71, 132->130
TestDiplomacy.py      37      1      2      1    95%   57
--------------------------------------------------------------
TOTAL                132      3     52      6    95%
"""