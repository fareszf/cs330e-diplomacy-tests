#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ------------
    # solve
    # ------------


    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("C London Support B\nA Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
    def test_solve_3(self):
        r = StringIO("B Barcelona Move Madrid\nA Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")
        
    def test_solve_4(self):
        r = StringIO("A KualaLumpur Hold\nB Sydney Move KualaLumpur\nC Taipei Move KualaLumpur\nD Osaka Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB KualaLumpur\nC [dead]\nD Osaka\n")
        
    def test_solve_5(self):
        r = StringIO("A NewYork Hold\nB HongKong Move NewYork\nC Tokyo Support B\nD Taipei Move Tokyo\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_6(self):
        r = StringIO("A Seville Support B\nB Barcelona Support A\nC Paris Move Seville\nD Porto Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_solve_8(self):
        r = StringIO("A Shanghai Hold\nB Taipei Hold\nC Seoul Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Shanghai\nB Taipei\nC Seoul\n")
        
    def test_solve_9(self):
        r = StringIO("A Shanghai Support B\nB Taipei Move Seoul\nC Seoul Hold\nD Manila Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Shanghai\nB Seoul\nC [dead]\nD Manila\n")
        
    def test_solve_10(self):
        r = StringIO("A Rome Hold\nB Venice Move London\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Rome\nB [dead]\nC [dead]\n")
        

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()
