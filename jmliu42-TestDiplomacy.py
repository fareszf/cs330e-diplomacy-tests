#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Quebec Hold\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i,  "A")
        self.assertEqual(j, "Quebec")
        self.assertEqual(k, "Hold")
        self.assertIsNone(l)

    def test_read_2(self):
        s = "B Liverpool Move London\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "B")
        self.assertEqual(j, "Liverpool")
        self.assertEqual(k, "Move")
        self.assertEqual(l, "London")

    def test_read_3(self):
        s = "C Paris Support Marseille\n"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i,  "C")
        self.assertEqual(j, "Paris")
        self.assertEqual(k, "Support")
        self.assertEqual(l, "Marseille")
    # -----
    # diplomacy_eval
    # -----
    def test_eval_1(self):
        actions = [("A", "NewYorkCity", "Hold", None), ("B", "Albany", "Move", "NewYorkCity")]
        i = diplomacy_eval(actions)
        self.assertEqual(i,  [("A", "[dead]"),("B", "[dead]")])
    
    def test_eval_2(self):
        actions = [("A", "NewYorkCity", "Hold", None), ("B", "Albany", "Move", "NewYorkCity"),
                   ("C", "Pittsburgh", "Support", "A")]
        i = diplomacy_eval(actions)
        self.assertEqual(i,  [("A", "NewYorkCity"),("B", "[dead]"),("C","Pittsburgh")])
        
    def test_eval_3(self):
        actions = [("A", "NewYorkCity", "Hold", None), ("B", "Albany", "Move", "NewYorkCity"),
                   ("C", "Pittsburgh", "Support", "B"),("D", "Buffalo", "Move", "Albany")]
        i = diplomacy_eval(actions)
        self.assertEqual(i,  [("A", "[dead]"),("B", "NewYorkCity"),
                              ("C","Pittsburgh"),("D","Albany")])
    def test_eval_4(self):
        actions = [("A", "Austin", "Hold", None), ("B", "SanAntonio", "Move", "Austin"),
                   ("C", "Houston", "Support", "A"),("D", "Dallas", "Move", "Houston"), 
                   ("E", "Arlington", "Support", "C")]
        i = diplomacy_eval(actions)
        self.assertEqual(i,  [("A", "[dead]"),("B", "[dead]"),
                              ("C","Houston"),("D","[dead]"), ("E","Arlington")])


    # -----
    # print
    # -----


    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [("A","[dead]"), ("B","London")])
        self.assertEqual(w.getvalue(), "A [dead]\nB London\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, [("A","SanAntonio"), ("B","Dallas"), ("C", "Houston")])
        self.assertEqual(w.getvalue(), "A SanAntonio\nB Dallas\nC Houston\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, [("A","[dead]"), ("B","[dead]"), ("C", "[dead]")])
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A LosAngeles Hold\nB Sacremento Move LosAngeles\nC SanDiego Move Sacremento\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Sacremento\n")

    def test_solve_2(self):
        r = StringIO("A Atlanta Move Athens\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Athens\n")

    def test_solve_3(self):
        r = StringIO("A Berlin Support C\nC Frankfurt Move Dresden\nB Dresden Move Hamburg\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Berlin\nB Hamburg\nC Dresden\n")

# ----
# main
# ----


if __name__ == "__main__": #pragma: no cover
    main()