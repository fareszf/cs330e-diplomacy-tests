#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, diplomacy_locations


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n"
        c = diplomacy_read(s)
        self.assertEqual(c, [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"],
                             ["C", "London", "Move", "Madrid"]])

    def test_read_2(self):
        s = "A Madrid Support B\nB Barcelona Hold\n"
        c = diplomacy_read(s)
        self.assertEqual(c, [["A", "Madrid", "Support", "B"], ["B", "Barcelona", "Hold"]])

    def test_read_3(self):
        s = "A Madrid Hold\n"
        c = diplomacy_read(s)
        self.assertEqual(c, [["A", "Madrid", "Hold"]])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        c = diplomacy_read("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        v = diplomacy_eval(c)
        self.assertEqual(v, ["A [dead]", "B [dead]", "C [dead]"])

    # def test_eval_2(self):
    #     v = diplomacy_eval(100, 200)
    #     self.assertEqual(v, 1)
    #
    # def test_eval_3(self):
    #     v = diplomacy_eval(201, 210)
    #     self.assertEqual(v, 1)
    #
    # def test_eval_4(self):
    #     v = diplomacy_eval(900, 1000)
    #     self.assertEqual(v, 1)

    # -----
    # locations
    # -----
    def test_locations(self):
        cmd = [["A", "Madrid", "Hold"], ["B", "Barcelona", "Move", "Madrid"], ["C", "London", "Move", "Madrid"]]
        c = diplomacy_locations(cmd)
        self.assertEqual(c[0], {"A": "Madrid", "B": "Barcelona", "C": "London"})
        self.assertEqual(c[1], {"A": "Madrid", "B": "Madrid", "C": "Madrid"})

    def test_locations_2(self):
        cmd = [["A", "Madrid", "Support", "B"], ["B", "Barcelona", "Hold"]]
        c = diplomacy_locations(cmd)
        self.assertEqual(c[0], {"A": "Madrid", "B": "Barcelona"})
        self.assertEqual(c[1], {"A": "Madrid", "B": "Barcelona"})

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        # inp = diplomacy_read("A Madrid Hold")
        out = ["A [dead]", "B [dead]"]
        diplomacy_print(w, out)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    # -----
    # solve
    # -----

    # Taken from ed discussion
    # Corner case where army is supporting an army that is already supporting another
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin "
                     "Support D\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
        w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")

    # Uneven supports but the top ones are tied so everyone dies
    # Taken from project description
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin "
                     "Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
        w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    # Supporter is attacked
    # Taken from project description
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
        w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # Only one army
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
        w.getvalue(), "A Madrid\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
