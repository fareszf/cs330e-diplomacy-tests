#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
import unittest

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        
        s = "A Madrid Hold\n"
        v = diplomacy_read(s)
        self.assertEqual(v,  ['A', 'Madrid', 'Hold', None, True, 0, 0])
        

    def test_read_2(self):
        
        s = "A Madrid Support B\n"
        v = diplomacy_read(s)
        self.assertEqual(v,  ['A', 'Madrid', 'Support', 'B', True, 0, 0])
        

    def test_read_3(self):
        s = "A Madrid Move C\n"
        v = diplomacy_read(s)
        self.assertEqual(v,  ['A', 'Madrid', 'Move', 'C', True, 0, 0])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval([['A', 'Madrid', 'Hold', None, True, 0, 0], 
                            ['B', 'Barcelona', 'Move', 'Madrid', True, 0, 0], 
                            ['C', 'London', 'Move', 'Madrid', True, 0, 0], 
                            ['D', 'Paris', 'Support', 'B', True, 0, 0], 
                            ['E', 'Austin', 'Support', 'A', True, 0, 0]])
        self.assertEqual(list(v),  ['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin'])
        

    def test_eval_2(self):
        v = diplomacy_eval([['A', 'Tokyo', 'Hold', None, True, 0, 0], 
                            ['B', 'Shangai', 'Move', 'Tokyo', True, 0, 0], 
                            ['C', 'London', 'Move', 'Tokyo', True, 0, 0], 
                            ['D', 'Toronto', 'Support', 'B', True, 0, 0]])
        self.assertEqual(list(v),  ['A [dead]', 'B Tokyo', 'C [dead]', 'D Toronto'])

    def test_eval_3(self):
        v = diplomacy_eval([])
        self.assertEqual(list(v),  [])


    # -----
    # print
    # -----

    def test_print_1(self):
        v = iter(['A [dead]', 'B [dead]', 'C [dead]', 'D Paris', 'E Austin'])
        w = StringIO()
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
        

    def test_print_2(self):
        v = iter(['A [dead]', 'B Tokyo', 'C [dead]', 'D Toronto'])
        w = StringIO()
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), "A [dead]\nB Tokyo\nC [dead]\nD Toronto\n")

    def test_print_3(self):
        v = None
        w = StringIO()
        diplomacy_print(w, v)
        self.assertEqual(w.getvalue(), '')

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")


    def test_solve_2(self):
        r = StringIO("A Tokyo Hold\nB Shanghai Move Tokyo\nC London Move Tokyo\nD Toronto Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Tokyo\nC [dead]\nD Toronto\n")
        
    def test_solve_3(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
