#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve, organize

class TestDiplomacy(TestCase):

    def test_eval_1(self):
        r = diplomacy_read(StringIO("A Boston Hold\nB Austin Move Boston\nC Houston Support B\n"))
        d = organize(r)
        l = diplomacy_eval(d)
        self.assertEqual(l, ['A [dead]', 'B Boston', 'C Houston'])

    def test_eval_2(self):
        r = diplomacy_read(StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Paris Move Madrid\nE Rome Support D"))
        d = organize(r)
        l = diplomacy_eval(d)
        self.assertEqual(l, ['A [dead]', 'B [dead]', 'C London', 'D [dead]', 'E Rome'])

    def test_eval_3(self):
        r = diplomacy_read(StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Tokyo\nD Paris Support A\nE Austin Support B"))
        d = organize(r)
        l = diplomacy_eval(d)
        self.assertEqual(l, ['A [dead]', 'B [dead]', 'C Tokyo', 'D Paris', 'E Austin'])

    def test_solve_1(self):
        r = StringIO("A Boston Hold\nB Austin Move Boston\nC Houston Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Boston\nC Houston\n")

    def test_solve_2(self):
        r = StringIO("A Paris Hold\nB Madrid Move Paris\nC NewYork Support B\nD Munich Move NewYork\n" )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Beijing Hold\nB Moscow Support A\nC Chicago Move Beijing\nD London Support C\n" )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Moscow\nC [dead]\nD London\n")

    def test_solve_4(self):
        r = StringIO("A Beijing Hold\nB Barcelona Move Beijing\nC Chicago Support B\nD Seattle Move Beijing\nE Seoul Support D\n" )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Chicago\nD [dead]\nE Seoul\n")



# ----
# main
# ----

if __name__ == "__main__": 
    main()

"""#pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          77      0     56      3    98%   36->38, 48->42, 66->60
TestDiplomacy.py      39      0      0      0   100%
--------------------------------------------------------------
TOTAL                116      0     56      3    98%
"""