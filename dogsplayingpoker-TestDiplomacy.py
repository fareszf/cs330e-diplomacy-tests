#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):

    # ----
    # solve
    # ----
    def test_solve_1(self):
        r = StringIO("A Winnipeg Hold\nB Montreal Move Winnipeg\nC Toronto Move Winnipeg\nD Vancouver Support B\nE Ottawa Support A")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Vancouver\nE Ottawa\n")


    def test_solve_2(self):
        r = StringIO("A Winnipeg Hold")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Winnipeg\n")


    def test_solve_3(self):
        r = StringIO("A Winnipeg Hold\nB Montreal Move Winnipeg\nC Toronto Support B")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Winnipeg\nC Toronto\n")


    def test_solve_4(self):
        r = StringIO("A Winnipeg Hold\nB Montreal Move Winnipeg")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")


    def test_solve_5(self):
        r = StringIO("A Winnipeg Hold\nB Montreal Move Winnipeg\nC Toronto Support B\nD Ottawa Move Toronto")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


    def test_solve_6(self):
        r = StringIO("A Winnipeg Hold\nB Montreal Move Winnipeg\nC Toronto Move Winnipeg")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")


    def test_solve_7(self):
        r = StringIO("A Winnipeg Hold\nB Montreal Move Winnipeg\nC Toronto Move Winnipeg\nD Vancouver Support B")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Winnipeg\nC [dead]\nD Vancouver\n")



    def test_solve_8(self):
        r = StringIO("A Winnipeg Hold\nB Montreal Move Winnipeg\nC Toronto Support B\nD Ottawa Move Toronto\nE Vancouver Support C")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Toronto\nD [dead]\nE Vancouver\n")


    def test_solve_9(self):
        r = StringIO("A Winnipeg Move Montreal\nB Montreal Move Berlin\nC Toronto Support A\nD Vancouver Support B")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Montreal\nB Berlin\nC Toronto\nD Vancouver\n")
# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
