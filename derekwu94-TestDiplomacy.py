#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Testdiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, army

# -----------
# Testdiplomacy
# -----------


class Testdiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_case1(self):
        result = diplomacy_read("A Berlin Hold")
        self.assertEqual(result[0], "A")
        self.assertEqual(result[1].location, "Berlin")
        self.assertEqual(result[1].supporters, 0)
        self.assertTrue(result[1].alive)

    def test_read_case2(self):
        # Test for 'Support' command
        result = diplomacy_read("A Munich Support A3")
        self.assertEqual(result[0], "A")
        self.assertEqual(result[1].location, "Munich")
        self.assertEqual(result[1].supporting, "A3")
        
    def test_read_case3(self):
        result = diplomacy_read("A Vienna Moves Budapest")
        self.assertEqual(result, None)

        
        
    def test_invalid_command(self):
        # Test for invalid command where assert should be triggered
        with self.assertRaises(AssertionError):
            diplomacy_read("A Vienna Move")



     

    # ----
    # eval
    # ----

    def test_eval_case1(self):
        armies = {
            "A": army(name="A", location="Madrid")
        }
        
        expected_location = "Madrid"
        expected_alive_status = True
        
        result = diplomacy_eval(armies)
        
        self.assertEqual(result["A"].location, expected_location)
        self.assertEqual(result["A"].alive, expected_alive_status)

        
    def test_eval_case2(self):
        armies = {}
        armies['A'] = army('A', 'Madrid')
        armies['B'] = army('B', 'Barcelona')
        armies['C'] = army('C', 'London', 'B')
        armies["B"].location = "Madrid"  
        result = diplomacy_eval(armies)
        
        self.assertFalse(result["A"].alive)
        self.assertEqual(str(result["A"]), "A [dead]")
        self.assertEqual(str(result["B"]), "B Madrid")
        self.assertEqual(str(result["C"]), "C London")


    def test_eval_case3(self):
        armies = {}
        armies['A'] = army('A', 'Madrid')
        armies['B'] = army('B', 'Barcelona')
        armies["A"].location = "Madrid"  
        armies["B"].location = "Madrid"  
        
        result = diplomacy_eval(armies)


        self.assertFalse(result["A"].alive)
        self.assertEqual(str(result["A"]), "A [dead]")
        self.assertEqual(str(result["B"]), "B [dead]")

    # -----
    # print
    # -----

    def test_print_case1(self):
        writer = StringIO()

        armies = {
            'Army1': army(name='Army1', location='Berlin'),
            'Army2': army(name='Army2', location='Munich')
        }
        writer = StringIO()
        diplomacy_print(writer, armies)
        output = writer.getvalue()
        writer.close()

        expected_output = "Army1 Berlin\nArmy2 Munich\n"
        self.assertEqual(output, expected_output)

    def test_print_case2(self):
        writer = StringIO()

        armies = {
            'Army1': army(name='Army1', location='Barcelona'),
            'Army2': army(name='Army2', location='Paris'),
            'Army3': army(name='Army3', location='Madrid')
        }
        writer = StringIO()
        diplomacy_print(writer, armies)
        output = writer.getvalue()
        writer.close()

        expected_output = "Army1 Barcelona\nArmy2 Paris\nArmy3 Madrid\n"
        self.assertEqual(output, expected_output)
    
    def test_print_case3(self):
        writer = StringIO()

        armies = {
            'Army1': army(name='Army1', location='Munich'),
            'Army2': army(name='Army2', location='Munich')
        }
        writer = StringIO()
        diplomacy_print(writer, armies)
        output = writer.getvalue()
        writer.close()

        expected_output = "Army1 Munich\nArmy2 Munich\n"
        self.assertEqual(output, expected_output)


    # -----
    # solve
    # -----

    def test_solve_case1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    def test_solve_case2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
    def test_solve_case3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
        
    def test_solve_case4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_case5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve_case6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
        
    def test_solve_case7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_solve_case8(self):
        r = StringIO("A Madrid Move Barcelona\nA Madrid Support C\nB London Hold\nC Paris Move Berlin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB London\nC Berlin\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch Testdiplomacy.py >  Testdiplomacy.out 2>&1


$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> Testdiplomacy.out



$ cat Testdiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
diplomacy.py          12      0      2      0   100%
Testdiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
