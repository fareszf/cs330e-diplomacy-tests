import unittest
import glob
from Diplomacy import diplomacy_solve

class TestDiplomacy(unittest.TestCase):

    def test_combined_cases(self):
        """
        Test all cases using RunDiplomacy*.in and RunDiplomacy*.out files.
        """
        input_files = sorted(glob.glob("RunDiplomacy*.in"))
        
        for input_file in input_files:
            # Generate the corresponding output file name
            output_file = input_file.replace(".in", ".out")

            with open(input_file, "r") as f:
                input_data = f.readlines()

            with open(output_file, "r") as f:
                expected_output = f.read().strip()

            # Run diplomacy_solve and compare the result
            result = diplomacy_solve(input_data).strip()
            self.assertEqual(result, expected_output, f"Failed for {input_file}")


if __name__ == '__main__':
    unittest.main()
