#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----


    def test_solve_1(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid \nC London Move Madrid \nD Paris Support B \nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")



    def test_solve_3(self):
        r = StringIO("A Madrid Hold \nB Barcelona Move Madrid \nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
       # print(w.getvalue())
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")



# ----
# main
# ----
if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""



"""
$ coverage run --branch TestDiplomacy.py > TestDiplomacy.out 2>&1

$ cat TestDiplomacy.out
...
----------------------------------------------------------------------
Ran 3 tests in 0.003s

OK

$ coverage report -m
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          53      1     34      1    98%   35
TestDiplomacy.py      21      0      0      0   100%
--------------------------------------------------------------
TOTAL                 74      1     34      1    98%

$ cat TestDiplomacy.out
...
----------------------------------------------------------------------
Ran 3 tests in 0.003s

OK

"""

