#!/usr/bin/env python3

# -----------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C)
# Glenn P. Downing
# -----------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, Army


# -------------
# TestDiplomacy
# -------------


class TestDiplomacy (TestCase):
    """
    Defined class for Diplomacy.py unit tests.
    """
    # ----
    # read
    # ----

    def test_read_1(self):
        """
        Unit test for the diplomacy_read function with a 3 string input.
        """
        s = "A Madrid Hold"
        i = diplomacy_read(s)
        self.assertEqual(i,  ["A","Madrid","Hold", None])

    def test_read_2(self):
        """
        Unit test for the diplomacy_read function with a 4 string input.
        """
        s = "A Austin Move Dallas"
        i = diplomacy_read(s)
        self.assertEqual(i,  ["A","Austin","Move","Dallas"])

    def test_read_3(self):
        """
        Unit test for the diplomacy_read function when input is not 3 or 4 string input.
        """
        s1 = "A Austin"
        with self.assertRaises(ValueError):
            diplomacy_read(s1)

        s2 = "A Chicago Move Miamin Hold"
        with self.assertRaises(ValueError):
            diplomacy_read(s2)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        Unit test for the diplomacy_eval function with a normal input.
        """
        moves = [
        ["A", "Madrid", "Hold", None],
        ["B", "Barcelona", "Move", "Madrid", None],
        ["C", "London", "Support", "B", None]
        ]
        result = diplomacy_eval(moves)
        self.assertEqual(result['A'].location, '[dead]')
        self.assertEqual(result['B'].location, 'Madrid')
        self.assertEqual(result['C'].location, 'London')

    def test_army_survival(self):
        """
        Test that armies survive or die based on their forces.
        """
        moves = [
            ['A', 'Madrid', 'Move', 'Barcelona'],
            ['B', 'Barcelona', 'Hold', None],
            ['C', 'Austin','Support','B']
        ]
        result = diplomacy_eval(moves)

        # Assuming army 'A' has higher forces than 'B', check if they survived
        self.assertEqual(result['A'].location, '[dead]')
        self.assertEqual(result['B'].location, 'Barcelona')
        self.assertEqual(result['C'].location, 'Austin')

    def test_eval_sorted_output(self):
        """
        Test that the result is sorted by army names.
        """
        moves = [
    ['D', 'Berlin', 'Support', 'B'],
    ['A', 'Madrid', 'Hold', None],
    ['C', 'London', 'Support', 'B'],
    ['B', 'Barcelona', 'Move', 'Madrid'],
    ['E', 'Rome', 'Support', 'A']
]
        result = diplomacy_eval(moves)
        army_names = list(result.keys())
        self.assertEqual(army_names, sorted(army_names))

    # # -----
    # # print
    # # -----

    def test_print(self):
        """
        Unit test for the diplomacy_print function for a single army input.
        """
        x = Army('A','Austin','Move','Dallas')
        x.location = 'Dallas'
        w = StringIO()
        diplomacy_print(w, x)
        self.assertEqual(w.getvalue(), "A Dallas\n")

    def test_print_dead_input(self):
        """
        Unit test for the diplomacy_print function with an army object with [dead] location attribute input.
        """
        x = Army('C','Dallas','Move','Houston')
        x.location = '[dead]'
        w = StringIO()
        diplomacy_print(w, x)
        self.assertEqual(w.getvalue(), "C [dead]\n")

    def test_print_multiple_calls(self):
        """
        Unit test for the diplomacy_print function with multiple function calls.
        """
        y = Army('B','Madrid','Hold', None)
        y.location = 'Madrid'
        x = Army('C','Austin','Move','Dallas')
        x.location = '[dead]'
        w = StringIO()
        diplomacy_print(w, y)
        diplomacy_print(w, x)
        self.assertEqual(w.getvalue(), "B Madrid\nC [dead]\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """Generic Scenario Acceptance Test"""
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Berlin\nC London Supports A\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Berlin\nC London\nD Paris\n")

    def test_solve_2(self):
        """Corner case with a single army"""
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_3(self):
        """Corner case where an army supports another army that is alreay supporting"""
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")

    def test_solve_4(self):
        """Corner case to test that if someone is attacked while trying to support, their support is invalidated"""
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Paris Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE Paris\n")

# ----
# main
# ----

class TestArmy(TestCase):
    """
    Defined class for unit tests specifically for the Army class.
    """

    def test_army_initialization(self):
        """
        Test initialization of an Army object.
        """
        army = Army('A', 'Madrid', 'Hold', None)
        army_b = Army('B', 'Dallas', 'Support', 'A')
        self.assertEqual(army.name, 'A')
        self.assertEqual(army.location, 'Madrid')
        self.assertEqual(army.action, 'Hold')
        self.assertIsNone(army.target)
        self.assertEqual(army.forces,0)
        self.assertEqual(army.can_support,True)
        self.assertEqual(army_b.action, 'Support')
        self.assertEqual(army_b.target, 'A')

    def test_army_move(self):
        """
        Test the move method of an Army object.
        """
        army = Army('A', 'Madrid', 'Move', 'Barcelona')
        army.move('Barcelona')
        self.assertEqual(army.location, 'Barcelona')

    def test_army_support(self):
        """
        Test the support method of an Army object.
        """
        army_a = Army('A', 'Madrid', 'Hold', None)
        army_b = Army('B', 'Barcelona', 'Support', 'A')
        army_c = Army('C', 'Austin', 'Support', 'B')
        army_c.can_support = False
        army_b.support(army_a)
        self.assertEqual(army_a.forces,1)
        army_c.support(army_b)
        self.assertEqual(army_b.forces,0)

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
................
----------------------------------------------------------------------
Ran 16 tests in 0.003s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          60      0     45      0   100%
TestDiplomacy.py     104      0      4      0   100%
--------------------------------------------------------------
TOTAL                164      0     49      0   100%

$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
................
----------------------------------------------------------------------
Ran 16 tests in 0.003s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          60      0     45      0   100%
TestDiplomacy.py     104      0      4      0   100%
--------------------------------------------------------------
TOTAL                164      0     49      0   100%
"""
