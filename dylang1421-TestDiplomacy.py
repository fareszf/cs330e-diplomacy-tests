#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from diplomacy import diplomacy_solve, diplomacy_read, diplomacy_write

class DiplomacyTest(TestCase):
    def test_read(self):
        s = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        lines = diplomacy_read(s)
        a = {"city": "Madrid", "action": "Hold", "action_entity": "", "attacked": False, "support": 0, "final": ""}
        b = {"city": "Barcelona", "action": "Move", "action_entity": "Madrid", "attacked": False, "support": 0, "final": ""}
        c = {"city": "London", "action": "Support", "action_entity": "B", "attacked": False, "support": 0, "final": ""}
        self.assertEqual(lines, {"A": a, "B": b, "C": c})

    def test_write(self):
        w = StringIO()

        a = {"city": "Madrid", "action": "Hold", "action_entity": "", "attacked": False, "support": 0, "final": "[dead]"}
        b = {"city": "Barcelona", "action": "Move", "action_entity": "Madrid", "attacked": False, "support": 0, "final": "Madrid"}
        c = {"city": "London", "action": "Support", "action_entity": "B", "attacked": False, "support": 0, "final": "London"}
        lines = {"A": a, "B": b, "C": c}

        diplomacy_write(w, lines)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Denmark Move London\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF NewYork Move Boston\n")
        w = StringIO()

        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\nF Boston\n")


if __name__ == "__main__": # pragma: no cover
    main()