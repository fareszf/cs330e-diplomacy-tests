# TestDiplomacy.py
# write unit tests that test corner cases and failure cases until you ahve 3 tests for the function diplomacy_solve()

from io import StringIO
from unittest import main, TestCase

from Diplomacy import parse_input, process_action, resolve_conflicts, finalize_results, diplomacy_solve

class TestDiplomacy(TestCase): 

# ---------------------------
# unit tests for parse_input()
# ---------------------------
    
    # generic test for parse_input
    def test_parse_input(self): 
        input_data = "Move ArmyA LocationA\nSupport ArmyB LocationB ArmyC\nHold ArmyD LocationD"
        input_stream = StringIO(input_data)
        expected = [
            "Move ArmyA LocationA",
            "Support ArmyB LocationB ArmyC",
            "Hold ArmyD LocationD"
        ]
        self.assertEqual(parse_input(input_stream), expected)
        
    #testing empty input
    def test_empty_input(self):
        input_data = ""
        input_stream = StringIO(input_data)
        self.assertEqual(parse_input(input_stream), [])    
    
    #testing empty line input
    def test_single_line_input(self):
        input_data = "A Madrid Hold"
        input_stream = StringIO(input_data)
        self.assertEqual(parse_input(input_stream), ["A Madrid Hold"])

# ---------------------------
# unit tests for process_action()
# ---------------------------
    
    # test for support
    def test_process_action_support(self):
        armies = {
            "ArmyA": ("LocationA", "Move", "LocationB"),
            "ArmyB": ("LocationA", "Hold", None),
            "ArmyC": ("LocationB", "Support", "ArmyA"),
        }
        conflicts = {
            "LocationA": ["ArmyA", "ArmyB"],
            "LocationB": ["ArmyC"]
        }
        support_counts = {"ArmyA": 1, "ArmyC": 1}
        
        results = resolve_conflicts(conflicts, armies, support_counts)
        self.assertEqual(results["ArmyA"], "LocationB")
        self.assertEqual(results["ArmyB"], "[dead]")
        self.assertEqual(results["ArmyC"], "LocationB")
    
    # test for moving
    def test_process_action_move(self):
        armies = {}
        conflicts = {}
        support_counts = {}
        action = "B Barcelona Move Madrid"
        process_action(action, armies, conflicts, support_counts)
        self.assertEqual(armies["B"], ("Barcelona", "Move", "Madrid"))
        self.assertEqual(conflicts["Madrid"], ["B"])
    
    # test for holding
    def test_process_action_hold(self):
        armies = {}
        conflicts = {}
        support_counts = {}
        action = "A Madrid Hold"
        process_action(action, armies, conflicts, support_counts)
        self.assertEqual(armies["A"], ("Madrid", "Hold", None))
        self.assertEqual(conflicts["Madrid"], ["A"])
        
# ---------------------------
# unit tests for resolve_conflicts()
# ---------------------------
    
    #testing single army
    def test_resolve_conflicts_single_army(self):
        conflicts = {"Madrid": ["A"]}
        armies = {"A": ("Madrid", "Hold", None)}
        support_counts = {}
        targets = {}
        results = resolve_conflicts(conflicts, armies, support_counts)
        self.assertEqual(results["A"], "Madrid")
        
    #testing no winners, everyone dead
    def test_resolve_conflicts_no_winner(self):
        conflicts = {"Madrid": ["A", "B"]}
        armies = {
            "A": ("Madrid", "Move", "Paris"),
            "B": ("Barcelona", "Move", "Madrid")
        }
        support_counts = {"A": 1, "B": 1}
        targets = {"A": "Paris", "B": "Madrid"}
        results = resolve_conflicts(conflicts, armies, support_counts)
        self.assertEqual(results["A"], "[dead]")
        self.assertEqual(results["B"], "[dead]")
        
    def test_resolve_conflicts(self):
        armies = {
            "ArmyA": ("LocationA", "Move", "LocationB"),
            "ArmyB": ("LocationA", "Hold", None),
            "ArmyC": ("LocationB", "Support", "ArmyA"),
        }
        conflicts = {
            "LocationA": ["ArmyA", "ArmyB"],
            "LocationB": ["ArmyC"]
        }
        support_counts = {"ArmyA": 1, "ArmyC": 1}
        
        results = resolve_conflicts(conflicts, armies, support_counts)
        self.assertEqual(results["ArmyA"], "LocationB")
        self.assertEqual(results["ArmyB"], "[dead]")
        self.assertEqual(results["ArmyC"], "LocationB")

# ---------------------------
# unit tests for finalize_results()
# ---------------------------
    # with no conflicts
    def test_finalize_results_no_conflict(self):
        armies = {
            "A": ("Madrid", "Hold", None),
            "B": ("Barcelona", "Move", "Madrid")
        }
        results = {
            "A": "Madrid",
            "B": "[dead]"
        }
        finalized = finalize_results(armies, results)
        self.assertEqual(finalized["A"], "Madrid")
        self.assertEqual(finalized["B"], "[dead]")
    
    # testing with support 
    def test_finalize_results_with_support(self):
        armies = {
            "A": ("Madrid", "Support", "B"),
            "B": ("Barcelona", "Move", "Madrid")
        }
        results = {
            "B": "Madrid"
        }
        finalized = finalize_results(armies, results)
        self.assertEqual(finalized["A"], "Madrid")
        self.assertEqual(finalized["B"], "Madrid")
    
    # testing where everyone is dead
    def test_finalize_results_all_dead(self):
        armies = {
            "A": ("Madrid", "Hold", None),
            "B": ("Barcelona", "Move", "Madrid")
        }
        results = {
            "A": "[dead]",
            "B": "[dead]"
        }
        finalized = finalize_results(armies, results)
        self.assertEqual(finalized["A"], "[dead]")
        self.assertEqual(finalized["B"], "[dead]")
        
# ---------------------------
# unit tests for diplomacy_solve()
# ---------------------------

    # testing basic functionality of diplomacy_solve()
    def test_diplomacy_solve_basic(self):
        input_data = """A Madrid Hold
B Barcelona Move Madrid
C London Support B"""
        input_stream = StringIO(input_data)
        output_stream = StringIO()
        diplomacy_solve(input_stream, output_stream)
        expected = "A [dead]\nB Madrid\nC London"
        self.assertEqual(output_stream.getvalue(), expected)
    
 
    
    # with a singular army
    def test_diplomacy_solve_single_army(self):
        input_data = "A Madrid Hold"
        input_stream = StringIO(input_data)
        output_stream = StringIO()
        diplomacy_solve(input_stream, output_stream)
        expected = "A Madrid"
        self.assertEqual(output_stream.getvalue(), expected)
        
    def test_diplomacy_solve_complex(self):
        input_data = """A Madrid Hold
B Barcelona Move Madrid
C London Move Madrid
D Paris Support B
E Austin Support A"""
  
        input_stream = StringIO(input_data)
        output_stream = StringIO()
        
        diplomacy_solve(input_stream, output_stream)
        
        expected = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin"
        self.assertEqual(output_stream.getvalue(), expected)

if __name__ == "__main__": # pragma: no cover
    main()
    
