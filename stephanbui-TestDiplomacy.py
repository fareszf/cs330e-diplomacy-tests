#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        r = StringIO("A Madrid Hold\n")
        i = diplomacy_read(r)
        self.assertEqual(i, ["A Madrid Hold"])

    def test_read_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n C London Support B\n")
        i = diplomacy_read(r)
        self.assertEqual(i, ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"])

    def test_read_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        i = diplomacy_read(r)
        self.assertEqual(i, ["A Madrid Hold", "B Barcelona Move Madrid"])

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(["A Madrid Hold"])
        self.assertEqual(v, {"A":"Madrid"})

    def test_eval_2(self):
        v = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"])
        self.assertEqual(v, {"A":"[dead]", "B":"Madrid", "C":"London"})
    
    def test_eval_3(self):
        v = diplomacy_eval(["A Madrid Hold", "B Barcelona Move Madrid"])
        self.assertEqual(v, {"A":"[dead]", "B":"[dead]"})

    # ----
    # print
    # ----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, {"A":"[dead]", "B":"[dead]", "C":"[dead]", "D":"Paris", "E":"Austin"})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {"A":"[dead]", "B":"Madrid", "C":"[dead]", "D":"Paris"})
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(w, {"A":"[dead]", "B":"[dead]", "C":"[dead]"})
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_empty(self): #test empty input
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
..............
----------------------------------------------------------------------
Ran 14 tests in 0.001s

OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
..............
----------------------------------------------------------------------
Ran 14 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          74      0     40      0   100%
TestDiplomacy.py      64      0      0      0   100%
--------------------------------------------------------------
TOTAL                138      0     40      0   100%
"""
