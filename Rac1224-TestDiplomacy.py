#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/Diplomacy.py
# Copyright (C)
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

import unittest
from io import StringIO
from Diplomacy import diplomacy_solve, armies, cities, supports

# ------------
# TestDiplomacy
# ------------

class TestDiplomacy(unittest.TestCase):
    def setUp(self):
        # Clear global dictionaries before each test
        armies.clear()
        cities.clear()
        supports.clear()

# ------------
# Stay
# ------------
    def test_hold(self):
        input_str = "A New York Hold"
        r = StringIO(input_str)
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A New York\n")

# ------------
# Move
# ------------

    def test_move(self):
        input_str = "A Bali Move Quezon"
        r = StringIO(input_str)
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Ouezon\n")

# ------------
# Support
# ------------

    def test_support(self):
        input_str = """A Dubai Support B\n B London Hold"""
        r = StringIO(input_str)
        w = StringIO()
        diplomacy_solve(r, w)
        expected = """A Dubai\n B London"""
        self.assertEqual(w.getvalue(), expected)

# ------------
# Scenorio 1
# ------------

    def test_conflict1(self):
        input_str = """A Jarkarta Move Penang\n B Penang Hold"""
        r = StringIO(input_str)
        w = StringIO()
        diplomacy_solve(r, w)
        expected = """A [dead]\n B [dead]"""
        self.assertEqual(w.getvalue(), expected)

# ------------
# Scenorio 2
# ------------

    def test_conflict2(self):
        input_str = """A Brussel Move Amsterdom\n B Amsterdom Hold\n C Rome Support B"""
        r = StringIO(input_str)
        w = StringIO()
        diplomacy_solve(r, w)
        expected = """A [dead]\n B Amsterdom\n C Rome"""
        self.assertEqual(w.getvalue(), expected)

# ------------
# Scenorio 3
# ------------

    def test_conflict3(self):
        input_str = """A Houston Move Boston\n B Boston Hold\n C Denver Support B\n D Seattle Support B"""
        r = StringIO(input_str)
        w = StringIO()
        diplomacy_solve(r, w)
        expected = """A [dead]\n B Boston\n C Denver\n D Seattle"""
        self.assertEqual(w.getvalue(), expected)

# ------------
# Scenorio 4
# ------------

    def test_conflict4(self):
        input_str = """A Paris Move London\n B Berlin Move London\n C Rome Move London"""
        r = StringIO(input_str)
        w = StringIO()
        diplomacy_solve(r, w)
        expected = """A [dead]\n B [dead]\nC [dead]"""
        self.assertEqual(w.getvalue(), expected)

# ------------
# Scenorio 5
# ------------

    def test_conflict5(self):
        input_str = """A TaiPei Move Bangkok\n B Shanghai Support A\n C Rome Move Bangkok\n D Kuala Lumpur Support C\n E Hanoi Hold"""
        r = StringIO(input_str)
        w = StringIO()
        diplomacy_solve(r, w)
        expected = """A [dead]\n B Shanghai\n C [dead]\n D Kuala Lumpur\n E Hanoi"""
        self.assertEqual(w.getvalue(), expected)

if __name__ == '__main__':
    unittest.main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
