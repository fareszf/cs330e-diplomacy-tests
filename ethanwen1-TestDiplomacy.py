#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy, extract, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # read/write 
    # r = StringIO
    # w= StringIO
    # diplomacy_solve(r, w)
    # ----
    def test_diplomacy_solve_1(self):
        # s = ["A Madrid Hold"]
        r = StringIO("A Madrid Hold")
        w = StringIO()
        # armies = extract(s)
        # print(armies)
        diplomacy_solve(r, w)
        # output = diplomacy_solve(armies)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_diplomacy_solve_2(self):
        # s = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        # armies = extract(s)
        # print(armies)
        diplomacy_solve(r, w)
        # output = diplomacy_solve(armies)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_diplomacy_solve_3(self):
        # s = ["A Madrid Hold", "B Barcelona Move Madrid"]
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        # armies = extract(s)
        # print(armies)
        diplomacy_solve(r, w)
        # output = diplomacy_solve(armies)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_diplomacy_solve_4(self):
        # s = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"]
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        # armies = extract(s)
        # output = diplomacy_solve(armies)
        # print(armies)
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_solve_5(self): 
        # s = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid"]
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        # armies = extract(s)
        # output = diplomacy_solve(armies)
        # print(armies)
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_diplomacy_solve_6(self): 
        # s = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B"]
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        # armies = extract(s)
        # output = diplomacy_solve(armies)
        # print(armies)
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_diplomacy_solve_7(self): 
        # s = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Austin Support A"]
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        # armies = extract(s)
        # output = diplomacy_solve(armies)
        # print(armies)
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_diplomacy_solve_8(self):
        r = StringIO("A Madrid Support B\nB Dallas Move Paris\nC Paris Hold\nD Seattle Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Paris\nC [dead]\nD Seattle\n")
    
    def test_diplomacy_solve_9(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Berlin\nC London Support A\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Berlin\nC London\nD Paris\n")
    
    # default failure case
    def test_diplomacy_solve_10(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_diplomacy_solve_11(self): 
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\nE Paris Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE Paris\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
(base) ➜  diplomacy_project git:(dev) ✗ coverage run --branch TestDiplomacy.py > TestDiplomacy.out 2>&1

(base) ➜  diplomacy_project git:(dev) ✗ cat TestDiplomacy.out
...........
----------------------------------------------------------------------
Ran 11 tests in 0.001s

OK

(base) ➜  diplomacy_project git:(dev) ✗ coverage report -m >> TestDiplomacy.out
(base) ➜  diplomacy_project git:(dev) ✗ cat TestDiplomacy.out
...........
----------------------------------------------------------------------
Ran 11 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          64      1     46      3    96%   47, 73->exit, 78->70
TestDiplomacy.py      61      0      0      0   100%
--------------------------------------------------------------
TOTAL                125      1     46      3    98%
"""
