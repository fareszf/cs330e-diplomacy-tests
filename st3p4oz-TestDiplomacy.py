#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    # def test_read(self):
    #     s = "1 10\n"
    #     i, j = diplomacy_read(s)
    #     self.assertEqual(i,  1)
    #     self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    """ def test_eval_1(self):
        v = diplomacy_eval(1, 10)
        self.assertEqual(v, 1)

    def test_eval_2(self):
        v = diplomacy_eval(100, 200)
        self.assertEqual(v, 1)

    def test_eval_3(self):
        v = diplomacy_eval(201, 210)
        self.assertEqual(v, 1)

    def test_eval_4(self):
        v = diplomacy_eval(900, 1000)
        self.assertEqual(v, 1) """

    # -----
    # print
    # -----

    

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Houston Hold\n B SanAntonio Move Houston")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")
        
    def test_solve_2(self):
        r = StringIO("A Houston Hold\n B SanAntonio Move Houston\n C Austin Move Houston\n D Dallas Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Houston\nD Dallas\n")
        
    def test_solve_3(self):
        r = StringIO("A Houston Support B\n B SanAntonio Move Houston\n C Austin Move SanAntonio")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC SanAntonio\n")
        
    def test_solve_4(self):
        r = StringIO("A Houston Hold\nB SanAntonio Hold\n C Houston Move SanAntonio\nD Austin Support B\n E Dallas Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Houston\nB SanAntonio\nC [dead]\nD Austin\nE Dallas\n")
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin Support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")   
        
    
    def test_solve_6(self):
        r = StringIO("A Houston Move Austin\nB SanAntonio Move Austin\n C Laredo Support A\nD Austin Move Laredo\nE Boise Support C\nF Florida Move Boise")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")
         
    

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
