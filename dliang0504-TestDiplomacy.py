#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):

    # -----
    # solve
    # -----
    def test_solve_empty_input(self):
        r = StringIO("\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve_invalid_action(self):
        r = StringIO("A Madrid Dream\n")
        w = StringIO()
        with self.assertRaises(ValueError):
            diplomacy_solve(r, w)

    def test_solve_move_no_target(self):
        r = StringIO("A Madrid Move\n")
        w = StringIO()
        with self.assertRaises(ValueError):
            diplomacy_solve(r, w)

    def test_solve_support_no_target(self):
        r = StringIO("A London Support\n")
        w = StringIO()
        with self.assertRaises(ValueError):
            diplomacy_solve(r, w)

    def test_solve_missing_action(self):
        r = StringIO("A Madrid\n")
        w = StringIO()
        with self.assertRaises(ValueError):
            diplomacy_solve(r, w)

    def test_solve_simple_hold(self):
        r = StringIO("A Paris Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\n")

    def test_solve_conflict(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_support(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_support_cut(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        )

    def test_solve_multiple_attackers(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "A [dead]\nB [dead]\nC [dead]\n"
        )

    def test_solve_no_conflict(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(),
            "A Madrid\nB Barcelona\nC London\n"
        )
        
# ----
# main
# ----

if __name__ == "__main__": 
    main()  # pragma: no cover
