from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------

class TestDiplomacy (TestCase):
    
    def test_diplomacy_solve_1 (self):
        # test the simplest case with one army holding
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_diplomacy_solve_2 (self):
        # test the case with one army attacking another while being supported by a third
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_diplomacy_solve_3 (self):
        # test the case when one army attacks another with no support
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_diplomacy_solve_4 (self):
        # two one on one matches with no support
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_solve_5 (self):
        # three way battle with no support for any army
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")   
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_diplomacy_solve_6 (self):
        # three way battle with support for one army
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")   
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_diplomacy_solve_7 (self):
        # more complex battle with 5 armies
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_diplomacy_solve_8 (self):
        # test with more armines and when one army supports an army that has not been registered yet
        r = StringIO("A Madrid Support H\nB Barcelona Hold\nC London Move Barcelona\nD Prague Hold\nE Berlin Hold\nF Rome Hold\nG Dublin Move Prague\nH Antwerp Move Prague")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD [dead]\nE Berlin\nF Rome\nG [dead]\nH Prague\n")

    def test_diplomacy_solve_9 (self):
        # test when armies are out of order
        r = StringIO("A Madrid Hold\nC London Move Madrid\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")


    
# ----
# main
# ----

if __name__ == "__main__":      # pragma: no cover
    main()

"""
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.tmp 2>&1
coverage report -m                        >> TestDiplomacy.tmp
cat TestDiplomacy.tmp
.........
----------------------------------------------------------------------
Ran 9 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          60      1     52      6    94%   32->34, 38, 41->22, 49->51, 65->63, 72->70
TestDiplomacy.py      49      0      0      0   100%
--------------------------------------------------------------
TOTAL                109      1     52      6    96%
"""