#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy(TestCase):
    # ----
    # read
    # ----
    def test_read1(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n"
        t = diplomacy_read(s)
        self.assertEqual(t, ["A Madrid Hold",
                             "B Barcelona Move Madrid", 
                             "C London Support B", 
                             "D Austin Move London"])

    def test_read2(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n"
        t = diplomacy_read(s)
        self.assertEqual(t, ["A Madrid Hold",
                             "B Barcelona Move Madrid", 
                             "C London Move Madrid", 
                             "D Paris Support B"])

    def test_read3(self):
        s = "A Madrid Hold\n"
        t = diplomacy_read(s)
        self.assertEqual(t,["A Madrid Hold"])

    def test_read4(self):
        s = "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n"
        t = diplomacy_read(s)
        self.assertEqual(t, ["A Madrid Hold",
                             "B Barcelona Move Madrid", 
                             "C London Support B"])
        
    def test_read5(self):
        s = "A Madrid Hold\nB Madrid Hold\nC Madrid Hold\n"
        t = diplomacy_read(s)
        self.assertEqual(t, ["A Madrid Hold",
                             "B Madrid Hold", 
                             "C Madrid Hold"])

    # ----
    # eval
    # ----
        
    def test_eval_1(self):
        t = diplomacy_eval(["A Madrid Hold",
                             "B Barcelona Move Madrid", 
                             "C London Support B", 
                             "D Austin Move London"])
        self.assertEqual(t, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_eval_2(self):
        t = diplomacy_eval(["A Madrid Hold",
                             "B Barcelona Move Madrid", 
                             "C London Move Madrid", 
                             "D Paris Support B"])
        self.assertEqual(t, "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    
    def test_eval_3(self):
        t = diplomacy_eval(["A Madrid Hold"])
        self.assertEqual(t, "A Madrid\n")

    def test_eval_4(self):
        t = diplomacy_eval(["A Madrid Hold",
                             "B Barcelona Move Madrid", 
                             "C London Support B"])
        self.assertEqual(t, "A [dead]\nB Madrid\nC London\n")

    def test_eval_5(self):
        t = diplomacy_eval(["A Madrid Hold",
                             "B Madrid Hold", 
                             "C Madrid Hold"])
        self.assertEqual(t, "A [dead]\nB [dead]\nC [dead]\n")

    # -----
    # print
    # -----

    def test_print1(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print2(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_print3(self):
        w = StringIO()
        diplomacy_print(w, "A Madrid\n")
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print4(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB Madrid\nC London\n")
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_print5(self):
        w = StringIO()
        diplomacy_print(w, "A [dead]\nB [dead]\nC [dead]\n")
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    # -----
    # solve
    # -----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
        
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Florence Move Venice\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Venice\n")
        
    def test_solve3(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")
        
    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
        
    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Madrid Hold\nC Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF Boston Support B\nG Miami Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\nF Boston\nG Miami\n")
        



# ----
# main
# ----

if __name__ == "__main__":
    main()


""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1
$ coverage report -m                   >> TestDiplomacy.out
$ cat TestDiplomacy.tmp
......................
----------------------------------------------------------------------
Ran 22 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          60      0     38      0   100%
TestDiplomacy.py      96      0      0      0   100%
--------------------------------------------------------------
TOTAL                156      0     38      0   100%
"""