#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from Diplomacy import Army, parse_input, create_armies, supports, diplomacy_solve

class TestCollatz (TestCase):
    # ----
    # parse_input
    # ----

    def test_parse_input(self):  # 1-2 digits
        s = """
        A Madrid Move Berlin
        B Houston Move Madrid
        C Barcelona Move Madrid
        D Seattle Move Madrid
        E Beijing Support B
        F Shanghai Support C
        G Singapore Support D
        """
        expected_output = [
        ["A", "Madrid", "Move", "Berlin"],
        ["B", "Houston", "Move", "Madrid"],
        ["C", "Barcelona", "Move", "Madrid"],
        ["D", "Seattle", "Move", "Madrid"],
        ["E", "Beijing", "Support", "B"],
        ["F", "Shanghai", "Support", "C"],
        ["G", "Singapore", "Support", "D"]
]

        self.assertEqual(parse_input(s.strip().splitlines()), expected_output)

    def test_parse_input2(self):  # Only 1 line
        s = "A Madrid Move Paris"
        expected_output = [["A", "Madrid", "Move", "Paris"]]
        self.assertEqual(parse_input(s.strip().splitlines()), expected_output)
        

    def test_parse_input3(self):
        s = """
        Z Plano Hold
        A Austin Move Plano
        X London Move Plano
        Y Houston Support A
        """
        
        expected_output = [
            ["A", "Austin", "Move", "Plano"],
            ["X", "London", "Move", "Plano"],
            ["Y", "Houston", "Support", "A"],
            ["Z", "Plano", "Hold"]
        ]
        
        self.assertEqual(parse_input(s.strip().splitlines()), expected_output)

    # ----
    # create_armies
    # ----

    def test_create_armies_simple(self):
        parsed_lines = [
            ["A", "Austin", "Hold"],
            ["B", "Berlin", "Hold"]
        ]
        
        armies = create_armies(parsed_lines)
        
        self.assertEqual(len(armies), 2)
        self.assertTrue("A" in armies)
        self.assertTrue("B" in armies)
        self.assertEqual(armies["A"].action, "Hold")
        self.assertEqual(armies["B"].action, "Hold")
        self.assertIsNone(armies["A"].target)  # "Hold" actions should not have a target
        self.assertIsNone(armies["B"].target)  # "Hold" actions should not have a target

    def test_create_armies_move(self):
        parsed_lines = [
            ["A", "Austin", "Move", "Plano"],
            ["B", "Berlin", "Move", "Paris"]
        ]
        
        armies = create_armies(parsed_lines)
        
        self.assertEqual(len(armies), 2)
        self.assertTrue("A" in armies)
        self.assertTrue("B" in armies)
        self.assertEqual(armies["A"].action, "Move")
        self.assertEqual(armies["B"].action, "Move")
        self.assertEqual(armies["A"].target, "Plano")
        self.assertEqual(armies["B"].target, "Paris")

    def test_create_armies_support(self):
        parsed_lines = [
            ["A", "Austin", "Support", "B"],
            ["B", "Berlin", "Support", "C"]
        ]
        
        armies = create_armies(parsed_lines)
        
        self.assertEqual(len(armies), 2)
        self.assertTrue("A" in armies)
        self.assertTrue("B" in armies)
        self.assertEqual(armies["A"].action, "Support")
        self.assertEqual(armies["B"].action, "Support")
        self.assertEqual(armies["A"].target, "B")
        self.assertEqual(armies["B"].target, "C")


    # -----
    # supports
    # -----

    def test_supports_no_attack(self):
        armies = {
            "A": Army("A", "Austin", "Support", "Plano"),
            "B": Army("B", "Berlin", "Support", "Plano"),
            "C": Army("C", "Plano", "Support", "Austin"),
        }
        
        # No armies attacking, so all support should be valid
        support_cache = supports(armies)
        
        # Plano should have 2 supports, and Austin should have 1 support
        self.assertEqual(support_cache, {"C": 2, "A": 1})

    def test_supports_invalidated_by_attack(self):
        armies = {
            "A": Army("A", "Austin", "Support", "Plano"),
            "B": Army("B", "Berlin", "Support", "Plano"),
            "C": Army("C", "Chicago", "Move", "Austin"),  # Austin is attacked
            "D": Army("D", "Plano", "Support", "Austin"), # Plano supports Austin, but Austin's support is still nullified
        }
        
        # Austin is attacked by army "C", so the support for Plano should be invalidated
        support_cache = supports(armies)
        
        # Plano should have 1 support (rather than 2) from Berlin, and Austin should have 1 support from Plano
        self.assertEqual(support_cache, {"A": 1, "D": 1})

    def test_supports_no_support_actions(self):
        # Create armies where no one is performing "Support" actions
        armies = {
            "A": Army("A", "Austin", "Move", "Plano"),
            "B": Army("B", "Berlin", "Move", "Houston"),
            "C": Army("C", "Chicago", "Move", "Dallas"),
        }

        # No support actions, so the support cache should be empty
        support_cache = supports(armies)
        
        self.assertEqual(support_cache, {})

    def test_supports_conflict_with_winner(self):
        """
        Test a conflict where one army wins due to support.
        """
        armies = {
            "A": Army("A", "Madrid", "Hold"),
            "B": Army("B", "Barcelona", "Move", "Madrid"),
            "C": Army("C", "London", "Support", "B")
        }
        support_cache = supports(armies)
        self.assertEqual(support_cache, {"B": 1})
        
    # -----
    # diplomacy_solve
    # -----

    def test_single_army_holding(self):
        """
        Test a single army holding a city with no conflicts.
        """
        input_lines = ["A Madrid Hold"]
        expected = ["A Madrid"]
        result = diplomacy_solve(input_lines)
        self.assertEqual(result, expected)

    def test_conflict_with_winner(self):
        """
        Test a conflict where one army wins due to support.
        """
        input_lines = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Support B"
        ]
        expected = [
            "A [dead]",
            "B Madrid",
            "C London"
        ]
        result = diplomacy_solve(input_lines)
        self.assertEqual(result, expected)

    def test_conflict_with_tie(self):
        """
        Test a conflict where all armies die due to a tie in support.
        """
        input_lines = [
            "A Madrid Hold",
            "B Barcelona Move Madrid",
            "C London Move Madrid"
        ]
        expected = [
            "A [dead]",
            "B [dead]",
            "C [dead]"
        ]
        result = diplomacy_solve(input_lines)
        self.assertEqual(result, expected)



# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
