from io import StringIO
import unittest
from Diplomacy import Diplomacy, City, Army, Action


class TestDiplomacy(unittest.TestCase):

    # invalid input
    def test_invalid_army_name1(self):
        obj = Diplomacy()
        reader = ""  # empty name
        with self.assertRaises(ValueError):
            obj._diplomacy_validate_army(reader)

    def test_invalid_army_name2(self):
        obj = Diplomacy()
        reader = "AB"  # invalid because len > 1
        with self.assertRaises(ValueError):
            obj._diplomacy_validate_army(reader)

    def test_invalid_army_name3(self):
        obj = Diplomacy()
        reader = "a"  # invalid because it's lowercase
        with self.assertRaises(ValueError):
            obj._diplomacy_validate_army(reader)

    def test_invalid_action(self):
        obj = Diplomacy()
        reader = "A Austin Skibidi"  # invalid action
        with self.assertRaises(ValueError):
            obj._diplomacy_read_move(reader)

    def test_invalid_initial_city(self):
        obj = Diplomacy()
        reader = "Aust1n"  # invalid because it includes a number
        with self.assertRaises(ValueError):
            obj._diplomacy_validate_initial_city(reader)

    def test_invalid_action_format(self):
        obj = Diplomacy()
        reader = (
            "move"  # invalid because it doesn't start with uppercase then lowercase
        )
        with self.assertRaises(ValueError):
            obj._diplomacy_validate_action(reader)

    # read move
    def test_diplomacy_read_move1(self):
        obj = Diplomacy()

        reader = "B Madrid Move Austin"

        move = obj._diplomacy_read_move(reader)
        expected_move = {
            "army": "B",
            "initial_city": "Madrid",
            "action": Action.MOVE,
            "new_city": "Austin",
            "supported_army": None,
        }
        self.assertEqual(move, expected_move)

    def test_diplomacy_read_move2(self):
        obj = Diplomacy()
        reader = "A Austin"  # missing action
        with self.assertRaises(ValueError):
            obj._diplomacy_read_move(reader)

    def test_diplomacy_read_move3(self):
        obj = Diplomacy()
        reader = "A"  # missing city and action
        with self.assertRaises(ValueError):
            obj._diplomacy_read_move(reader)

    # read turn
    def test_diplomacy_read_turn1(self):
        obj = Diplomacy()
        reader = StringIO("A Austin Hold\nB Madrid Move Austin\nC Boston Support B\n")
        moves = obj._diplomacy_read_turn(reader)

        expected_moves = [
            {
                "army": "A",
                "initial_city": "Austin",
                "action": Action.HOLD,
                "new_city": None,
                "supported_army": None,
            },
            {
                "army": "B",
                "initial_city": "Madrid",
                "action": Action.MOVE,
                "new_city": "Austin",
                "supported_army": None,
            },
            {
                "army": "C",
                "initial_city": "Boston",
                "action": Action.SUPPORT,
                "new_city": None,
                "supported_army": "B",
            },
        ]
        self.assertEqual(moves, expected_moves)

    # this edge case fails because its not accounted for in Diplomacy.py
    def test_diplomacy_read_turn2(self):
        obj = Diplomacy()
        reader = StringIO("A Austin Support B\n")  # 'B' does not exist
        with self.assertRaises(ValueError):
            obj._diplomacy_read_turn(reader)

    # solve diplomacy
    def test_diplomacy_solve1(self):
        obj = Diplomacy()

        reader = StringIO("A Austin Hold\nB Madrid Move Austin\nC Boston Support B")
        writer = StringIO()

        obj.diplomacy_solve(reader, writer)

        res = writer.getvalue()
        expected_output = "A [dead]\nB Austin\nC Boston"

        self.assertEqual(res, expected_output)

    def test_diplomacy_solve2(self):
        obj = Diplomacy()

        reader = StringIO(
            "A Madrid Hold\n"
            "B Barcelona Move Madrid\n"
            "C London Move Madrid\n"
            "D Paris Support B\n"
            "E Austin Support A"
        )
        writer = StringIO()

        obj.diplomacy_solve(reader, writer)

        res = writer.getvalue()
        expected_output = "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin"

        self.assertEqual(res, expected_output)

    def test_diplomacy_solve3(self):
        obj = Diplomacy()

        reader = StringIO(
            "A Madrid Hold\n" "B Barcelona Move Madrid\n" "C London Move Madrid"
        )
        writer = StringIO()

        obj.diplomacy_solve(reader, writer)

        res = writer.getvalue()
        expected_output = "A [dead]\nB [dead]\nC [dead]"

        self.assertEqual(res, expected_output)


if __name__ == "__main__":  # pragma: no cover
    unittest.main()
