# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestCollatz
# -----------


# pylint: disable=line-too-long
# pylint: disable=too-many-public-methods
# pylint: disable=missing-function-docstring
class TestDiplomacy(TestCase):
    """
    Unit tests testing the diplomacy_solve(r,w) in Diplomacy.py
    """

    # -----
    # solve
    # -----

    def test_diplomacy_solve1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n E Dublin Support D"
        )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n"
        )

    def test_diplomacy_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")

    def test_diplomacy_solve3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Support A\nC Paris Move Madrid\nD Austin Support C"
        )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC [dead]\nD Austin\n")

    def test_diplomacy_solve_4(self):
        r = StringIO("A Madrid Move Paris\nB Paris Move Austin\nC Austin Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB Austin\nC Madrid\n")

    def test_diplomacy_solve5(self):
        r = StringIO("A Austin Support B\nB Waco Move Paris\nC Paris Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Austin\nB Paris\nC [dead]\n")


if __name__ == "__main__":
    main()


# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out

.....
----------------------------------------------------------------------
Ran 5 tests in 0.001s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          66      0     44      4    96%   30->26, 80->61, 81->83, 91->86
TestDiplomacy.py      33      2      2      1    91%   70-71
--------------------------------------------------------------
TOTAL                 99      2     46      5    95%

"""
