#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestDiplomacy.py
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, diplomacy_invalidate_support, diplomacy_find_victor
from io import StringIO
# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        self.assertEqual(diplomacy_read("A Madrid Hold"),['A', 'Madrid', 'Hold', None])

    def test_read_2(self):
        self.assertEqual(diplomacy_read('I love coding so much'), ['I', 'love','coding','so','much'])

    def test_read_3(self):
        try:
           diplomacy_read(55)
        except AttributeError as no:
            assert str(no) == "'int' object has no attribute 'split'"

    # ----
    # invalidate_support
    # ----

    def test_invalidate_support_1(self):
        enemy = "A"
        army_supports = {"B": 2, "C": 3}
        supported_army = {"A": "B"}
        
        diplomacy_invalidate_support(enemy, army_supports, supported_army)
        
        self.assertEqual(army_supports["B"], 1)
        self.assertNotIn("A", supported_army)

    def test_invalidate_support_2(self):
        enemy = "D"
        army_supports = {"B": 2, "C": 3}
        supported_army = {"A": "B"}
        
        diplomacy_invalidate_support(enemy, army_supports, supported_army)
        
        self.assertEqual(army_supports, {"B": 2, "C": 3})
        self.assertEqual(supported_army, {"A": "B"})


    def test_invalidate_support_3(self):
        enemy = "A"
        army_supports = {"B": 0}
        supported_army = {"A": "B"}
        
        diplomacy_invalidate_support(enemy, army_supports, supported_army)
        
        self.assertEqual(army_supports["B"], -1)
        self.assertNotIn("A", supported_army)

    def test_invalidate_support_4(self):
        enemy = "Z"  # Not in supported_army
        army_supports = {"B": 2, "C": 3}
        supported_army = {"A": "B"}
        
        diplomacy_invalidate_support(enemy, army_supports, supported_army)
        
        # Ensure that the dictionaries remain unchanged since the enemy is not found
        self.assertEqual(army_supports, {"B": 2, "C": 3})
        self.assertEqual(supported_army, {"A": "B"})



    # -----
    # eval
    # -----
    def test_eval_1(self):
        elements = ["A", "Houston", "Move", "Austin"]
        army_supports = {"A": 0}
        supported_army = {}
        armies_at_location = {
            "Houston": {"A"},
            "Austin": set()
        }
        location_of_army = {"A": "Houston"}
        
        diplomacy_eval(elements, army_supports, supported_army, armies_at_location, location_of_army)
        
        self.assertIn("A", armies_at_location["Austin"])
        self.assertEqual(location_of_army["A"], "Austin")
        

    def test_eval_2(self):
        elements = ["A", "Houston", "Support", "C"]
        army_supports = {"C": 0}
        supported_army = {}
        armies_at_location = {"Houston": set()}
        location_of_army = {"A": "Houston"}
        
        diplomacy_eval(elements, army_supports, supported_army, armies_at_location, location_of_army)
        
        self.assertEqual(army_supports["C"], 1)
        self.assertEqual(supported_army["A"], "C")
        self.assertEqual(army_supports["A"], 0)


    def test_eval_3(self):
        elements = ["A", "Houston", "Invalid", "Austin"]
        army_supports = {"A": 1, "B": 2}
        supported_army = {"B": "C"}
        armies_at_location = {
            "Houston": {"A"},
            "Austin": {"B"}
        }
        location_of_army = {"A": "Houston", "B": "Austin"}
        
        original_supported_army = supported_army.copy()
        original_armies_at_location = {k: v.copy() for k, v in armies_at_location.items()}
        original_location_of_army = location_of_army.copy()
        
        diplomacy_eval(elements, army_supports, supported_army, armies_at_location, location_of_army)
        
        self.assertEqual(army_supports["A"], 0)
        self.assertEqual(army_supports["B"], 2)
        
        self.assertEqual(supported_army, original_supported_army)
        self.assertEqual(armies_at_location, original_armies_at_location)
        self.assertEqual(location_of_army, original_location_of_army)

        
    # -----
    # find_victor
    # -----
    def test_find_victor_1(self):
        location = "Houston"
        army_supports = {"A": 3}
        armies_at_location = {"Houston": {"A"}}
        location_of_army = {"A": "Houston"}
        
        diplomacy_find_victor(location, army_supports, armies_at_location, location_of_army)
        
        self.assertEqual(location_of_army["A"], "Houston")

    def test_find_victor_2(self):
        location = "Austin"
        army_supports = {"A": 2, "B": 3, "C": 1}
        armies_at_location = {"Austin": {"A", "B", "C"}}
        location_of_army = {"A": "Austin", "B": "Austin", "C": "Austin"}
        
        diplomacy_find_victor(location, army_supports, armies_at_location, location_of_army)
        
        self.assertEqual(location_of_army["B"], "Austin")
        self.assertEqual(location_of_army["A"], "[dead]")
        self.assertEqual(location_of_army["C"], "[dead]")

    def test_find_victor_3(self):
        location = "Boston"
        army_supports = {"A": 2, "B": 2}
        armies_at_location = {"Boston": {"A", "B"}}
        location_of_army = {"A": "Boston", "B": "Boston"}
        
        diplomacy_find_victor(location, army_supports, armies_at_location, location_of_army)
    
        self.assertEqual(location_of_army["A"], "[dead]")
        self.assertEqual(location_of_army["B"], "[dead]")


    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        army = "A"
        location = "Houston"
        diplomacy_print(w, army, location)
        
        self.assertEqual(w.getvalue(), "A Houston\n")

    def test_print_2(self):
        w = StringIO()
        army = ""
        location = "Austin"
        diplomacy_print(w, army, location)
        
        self.assertEqual(w.getvalue(), " Austin\n")

    def test_print_3(self):
        w = StringIO()
        army = "B"
        location = ""
        diplomacy_print(w, army, location)
        
        # Check the output
        self.assertEqual(w.getvalue(), "B \n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        input_data = ["A Location1 Move Location2\n"]
        r = StringIO("".join(input_data))
        w = StringIO()
        
        diplomacy_solve(r, w)
        
        # Check that the output is correct
        self.assertEqual(w.getvalue(), "A Location2\n")

    def test_solve_2(self):
        input_data = [
            "A Houston Hold\nB Austin Move Houston\n"]
        r = StringIO("".join(input_data))
        w = StringIO()
        
        diplomacy_solve(r, w)
        
        # Check the output: Army C should win at Location2 due to support
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        input_data = [
            "A Houston Hold\nB Austin Hold\nC Boston Move Austin\n"
        ]
        r = StringIO("".join(input_data))
        w = StringIO()
        
        diplomacy_solve(r, w)
        
        # Both armies A and B should be dead, as there is a tie for Army C
        self.assertEqual(w.getvalue(), "A Houston\nB [dead]\nC [dead]\n")

        
# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
...................
----------------------------------------------------------------------
Ran 19 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          47      0     20      0   100%
TestDiplomacy.py     137      0      0      0   100%
--------------------------------------------------------------
TOTAL                184      0     20      0   100%
"""
