#!/usr/bin/env python3

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # ------
    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Paris Move Madrid\nC NewYork Hold\nD London Support A\n")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [Dead]\nC NewYork\nD London\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Paris Move Madrid\nC London Move Madrid\nD Barcelona Support A\nE Rome Support B\n")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [Dead]\nB [Dead]\nC [Dead]\nD Barcelona\nE Rome\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [Dead]\nB [Dead]\nC [Dead]\n")

    def test_solve_4(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")

        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [Dead]\nB [Dead]\nC [Dead]\nD Paris\nE Austin\n")
        
    def test_solve_5(self):
        r = StringIO(
            "A Nairobi Hold\nB Barcelona Move Nairobi\nC London Support B\nD Austin Move London\n"
        )
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A [Dead]\nB [Dead]\nC [Dead]\nD [Dead]\n"
        )

    def test_solve_6(self):
        r = StringIO(
            "A Nairobi Hold\nB Austin Move Nairobi\nC London Move Nairobi\nD Berlin Support A\n"
        )
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(), "A Nairobi\nB [Dead]\nC [Dead]\nD Berlin\n"
        )

# ----
# main
# ----

if __name__ == "__main__":
    main()
