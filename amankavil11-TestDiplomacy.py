from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):
    
    def test_diplomacy_solve_1 (self):
        # test the simplest case with one army holding
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_diplomacy_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_diplomacy_solve_3 (self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")   
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_diplomacy_solve_4 (self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    
# ----
# main
# ----

if __name__ == "__main__":      # pragma: no cover
    main()

"""
coverage run    --branch TestDiplomacy.py >  TestDiplomacy.tmp 2>&1
coverage report -m                        >> TestDiplomacy.tmp
cat TestDiplomacy.tmp
....
----------------------------------------------------------------------
Ran 4 tests in 0.000s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          53      2     46      4    92%   21->10, 24->26, 54->46, 68-70
TestDiplomacy.py      25      1      0      0    96%   41
--------------------------------------------------------------
TOTAL                 78      3     46      4    93%
"""
