from unittest import main, TestCase
from io import StringIO

from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):
    def test_solve1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Dublin support D")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\nE Dublin\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Paris Move London\nD Dublin Support A\nE Barcelona Move Dublin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD [dead]\nE [dead]\n")

    def test_solve3(self):
        r = StringIO("A Houston Move Austin\nB Austin Support A\nC Dallas Move Austin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

if __name__ == "__main__":
    main()
