#!/usr/bin/env python3

# -----------------------------------
# projects/diplomacy/TestDiplomacy.py
# Edward Renteria and Joy Ekechukwu
# -----------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_write, Game, City, Army

# -------------
# TestDiplomacy
# -------------

class TestDiplomacy (TestCase):
    
    # ----
    # read
    # ----
    
    def test_read_1(self):
        result = diplomacy_read("A Cairo Move ElChique")
        self.assertEqual(result, ("A", "Cairo", "Move", "ElChique"))
        
    def test_read_2(self):
        result = diplomacy_read("D Phoenix Hold")
        self.assertEqual(result, ("D", "Phoenix", "Hold", None))
        
    def test_read3(self):
        result = diplomacy_read("Z AfnfjFenEm Support B")
        self.assertEqual(result, ("Z", "AfnfjFenEm", "Support", "B"))

    # -----
    # solve
    # -----
    
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Cairo Move Dallas\nB Paris Hold\nC Boston Move Paris\nD Dallas Support C\nE Austin Support C")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Paris\nD [dead]\nE Austin\n") 
        
    # -----
    # write
    # -----
    
    def test_write_1(self):
        w = StringIO()
        diplomacy_write(w, "aaa")
        self.assertEqual(w.getvalue(), "aaa")
        
    def test_write_2(self):
        newGame = Game()
        w = StringIO()
        diplomacy_write(w, newGame)
        self.assertEqual(w.getvalue(), "")
        
    def test_write_3(self):
        newGame = Game()
        cityA = City("Houston")
        cityB = City("LosAngeles")
        armyA = Army("A", cityA, ("Hold"))
        armyB = Army("B", cityB, ("Support", "B"))
        newGame.cities = [cityA, cityB]
        newGame.armies = [armyA, armyB]
        
        w = StringIO()
        diplomacy_write(w, newGame)
        self.assertEqual(w.getvalue(), "A Houston\nB LosAngeles\n")
        
    # ----
    # Game
    # ----
    
    def test_game_init_1(self):
        newGame = Game()
        
        self.assertEqual(newGame.cities, [])
        self.assertEqual(newGame.armies, [])
    
    def test_game_init_2(self):
        newGame = Game()
        cityA = City("Houston")
        cityB = City("LosAngeles")
        armyA = Army("A", cityA, ("Hold"))
        armyB = Army("B", cityB, ("Support", "B"))
        newGame.cities.append(cityA)
        newGame.cities.append(cityB)
        newGame.armies.append(armyA)
        newGame.armies.append(armyB)
        
        self.assertEqual(newGame.cities, [cityA, cityB])
        self.assertEqual(newGame.armies, [armyA, armyB])
        
    # ----
    # City
    # ----
    
    def test_city_init_1(self):
        city = City("Dallas")
        armyA = Army("A", city, ("Hold"))
        armyB = Army("B", city, ("Hold"))
        
        self.assertEqual(city.name, "Dallas")
        self.assertEqual(city.armies[0], armyA)
        self.assertEqual(city.armies[1], armyB)
        
    def test_city_init_2(self):
        city = City("StPaul")
        
        self.assertEqual(city.name, "StPaul")
        self.assertEqual(len(city.armies), 0)

    def test_city_init_3(self):
        city = City("Paris")
        armyA = Army("A", city, ("Hold"))
        armyB = Army("B", city, ("Hold"))
        armyC = Army("C", city, ("Hold"))
        
        self.assertEqual(city.name, "Paris")
        self.assertEqual(city.armies[0], armyA)
        self.assertEqual(city.armies[1], armyB)
        self.assertEqual(city.armies[2], armyC)

    # ----
    # Army
    # ----
    
    def test_army_init_1(self):
        army = Army("A", City("Madrid"), ("Hold"))
        self.assertEqual(army.name,"A")
        self.assertEqual(army.city.name, "Madrid")
        self.assertEqual(army.action, ("Hold"))
        self.assertEqual(army.city.armies[0], army)
        
    def test_army_init_2(self):
        army = Army("B", City("NewYork"), ("Support", "A"))
        self.assertEqual(army.name,"B")
        self.assertEqual(army.city.name, "NewYork")
        self.assertEqual(army.action, ("Support", "A"))
        self.assertEqual(army.city.armies[0], army)

if __name__ == "__main__":
    main()
    
""" #pragma: no cover
"""