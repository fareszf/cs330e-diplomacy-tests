#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_solve

class TestDiplomacy(TestCase):

    def test_single_hold(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_support_attack(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_equal_support_conflict(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_multiple_moves_with_support(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_invalid_support_due_to_attack(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_conflict_with_equal_support(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_support_nonexistent_army(self):
        r = StringIO("A Berlin Support X\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Berlin\n")

    def test_attacker_stronger_than_defender(self):
        r = StringIO(
            "A Berlin Move Paris\n"
            "B Paris Hold\n"
            "C Munich Support A\n"
        )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB [dead]\nC Munich\n")

    def test_attacker_weaker_than_defender(self):
        r = StringIO(
            "A Berlin Move Paris\n"
            "B Paris Hold\n"
            "C Munich Support B\n"
        )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Paris\nC Munich\n")

    def test_move_into_unoccupied_territory(self):
        r = StringIO("A Berlin Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\n")

    def test_multiple_attackers_varied_strengths(self):
        r = StringIO(
            "A Berlin Move Paris\n"
            "B Munich Move Paris\n"
            "C Paris Hold\n"
            "D Vienna Support A\n"
        )
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Paris\nB [dead]\nC [dead]\nD Vienna\n")


if __name__ == "__main__":  # pragma: no cover
    main()
