# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    def test_solve_1(self):
        r = StringIO("A Athens Hold\nB Boston Move Athens\nC Chicago Move Athens")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_2(self):
        r = StringIO("A Austin Hold\nB Barcelona Move Austin\nC Cairo Move Austin\nD Denver Support B\nE Edinburgh Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Denver\nE Edinburgh\n")

    def test_solve_3(self):
        r = StringIO("A Antwerp Hold\nB Bangalore Move Antwerp\nC Copenhagen Move Antwerp\nD Detroit Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Antwerp\nC [dead]\nD Detroit\n")
        
    def test_solve_4(self):
        r = StringIO("")
        w = StringIO()
        with self.assertRaises(AssertionError):
            diplomacy_solve(r, w)
    
    def test_solve_5(self):
        r = StringIO("A Arlington Hold\nB Bruges Support A\nC Casablanca Move Bruges")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Arlington\nB [dead]\nC [dead]\n")
        
    def test_solve_6(self):
        r = StringIO("A Ankara Move Boston\nB Berlin Move Boston\nC Columbus Support A\nD Doha Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Columbus\nD Doha\n")
        
    def test_solve_7(self):
        r = StringIO("A Anchorage Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Anchorage\n")
        
    def test_solve_8(self):
        r = StringIO("A Albuquerque Hold\nB Buffalo Move Albuquerque\nC Charleston Support B\nD Dhaka Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC Charleston\nD Dhaka\n")
        
    def test_solve_9(self):
        r = StringIO("A Albany Move Beirut\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Beirut\n")
        
    def test_solve_10(self):
        r = StringIO("A Austin Move Detroit\nB Antwerp Hold\nC Amsterdam Hold\nD Detroit Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Detroit\nB Antwerp\nC Amsterdam\nD Austin\n")
    def test_solve_11(self):
        r = StringIO("A Anaheim Move Dublin\nB Boise Move Dublin\nC Chattanooga Move Dublin\nD Dayton Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Dublin\nC [dead]\nD Dayton\n")

# ----
# main
# ----
if __name__ == "__main__":
    main()