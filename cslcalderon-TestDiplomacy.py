#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_solve, support, diplomacy_eval, check_current_location, diplomacy_print

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):

    # ----
    # check current locations
    # ----

    def test_current_location_1(self):
        army_dict = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                     "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0}}
        expected = {"Madrid": ["A", "B"]}
        result = check_current_location(army_dict)
        self.assertEqual(result, expected)

    def test_current_location_2(self):
        army_dict = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                     "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0}}
        expected = {"Madrid": ["A", "B"]}
        result = check_current_location(army_dict)
        self.assertEqual(result, expected)

    # # ----
    # # support
    # # ----

    def test_support_being_attacked(self):
        army_dict = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                     "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0},
                     "C": {"army": "C", "location": "Paris", "action": "Support", "target": "A",  "support": 0},
                     "D": {"army": "D", "location": "Prague", "action": "Move", "target": "Paris",  "support": 0}}

        expected = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                    "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0},
                    "C": {"army": "C", "location": "Paris", "action": "Support", "target": "A",  "support": 0},
                    "D": {"army": "D", "location": "Prague", "action": "Move", "target": "Paris",  "support": 0}}

        result = support(army_dict)
        self.assertEqual(result, expected)

    def test_no_supports(self):
        army_dict = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                     "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0}}
        expected = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                    "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0}}

        result = support(army_dict)
        self.assertEqual(result, expected)

    def test_unbalanced_supports(self):
        army_dict = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                     "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0},
                     "C": {"army": "C", "location": "Ham", "action": "Support", "target": "A",  "support": 0}}

        expected = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 1},
                    "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0},
                    "C": {"army": "C", "location": "Ham", "action": "Support", "target": "A",  "support": 0}}

        result = support(army_dict)
        self.assertEqual(result, expected)

    # # ----
    # # read
    # # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        result, army = diplomacy_read(s)
        expected = {"army": "A", "location": "Madrid",
                    "action": "Hold", "target": None, "support": 0}
        self.assertEqual(result, expected)
        self.assertEqual(army, "A")

    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        result, army = diplomacy_read(s)
        expected = {"army": "B", "location": "Barcelona",
                    "action": "Move", "target": "Madrid", "support": 0}
        self.assertEqual(result, expected)
        self.assertEqual(army, "B")

    def test_read_3(self):
        s = "C London Support B\n"
        result, army = diplomacy_read(s)
        expected = {"army": "C", "location": "London",
                    "action": "Support", "target": "B", "support": 0}
        self.assertEqual(result, expected)
        self.assertEqual(army, "C")

    def test_read_4(self):
        s = "D Paris Support A\n"
        result, army = diplomacy_read(s)
        expected = {"army": "D", "location": "Paris",
                    "action": "Support", "target": "A", "support": 0}
        self.assertEqual(result, expected)
        self.assertEqual(army, "D")

    # ----
    # eval
    # ----

    def test_eval_1(self):
        army_info = {"A": {"army": "A", "location": "Madrid", "action": "Move", "target": "Paris", "support": 0},
                     "B": {"army": "B", "location": "Paris", "action": "Move", "target": "Madrid",  "support": 0}}
        diplomacy_eval(army_info)
        expected = {"A": {"army": "A", "location": "Paris", "action": "Move", "target": "Paris", "support": 0},
                    "B": {"army": "B", "location": "Madrid", "action": "Move", "target": "Madrid",  "support": 0}}
        self.assertEqual(army_info, expected)

    def test_eval_2(self):
        army_info = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                     "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0},
                     "C": {"army": "C", "location": "Paris", "action": "Support", "target": "A",  "support": 0}}
        diplomacy_eval(army_info)
        expected = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 1},
                    "B": {"army": "B", "location": "[dead]", "action": "Move", "target": "Madrid",  "support": 0}, "C":
                    {"army": "C", "location": "Paris", "action": "Support", "target": "A",  "support": 0}}
        self.assertEqual(army_info, expected)

    def test_eval_3(self):
        army_info = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                     "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0},
                     "C": {"army": "C", "location": "Paris", "action": "Move", "target": "Madrid",  "support": 0}}
        diplomacy_eval(army_info)
        expected = {"A": {"army": "A", "location": "[dead]", "action": "Hold", "target": None, "support": 0},
                    "B": {"army": "B", "location": "[dead]", "action": "Move", "target": "Madrid",  "support": 0}, "C":
                    {"army": "C", "location": "[dead]", "action": "Move", "target": "Madrid",  "support": 0}}
        self.assertEqual(army_info, expected)

    def test_eval_4(self):
        army_info = {"A": {"army": "A", "location": "Madrid", "action": "Hold", "target": None, "support": 0},
                     "B": {"army": "B", "location": "Barcelona", "action": "Move", "target": "Madrid",  "support": 0},
                     "C": {"army": "C", "location": "Paris", "action": "Support", "target": "A",  "support": 0},
                     "D": {"army": "D", "location": "Dallas", "action": "Move", "target": "Paris",  "support": 0},
                     "E": {"army": "E", "location": "Rome", "action": "Move", "target": "Rome",  "support": 0}}
        diplomacy_eval(army_info)
        expected = {"A": {"army": "A", "location": "[dead]", "action": "Hold", "target": None, "support": 0},
                    "B": {"army": "B", "location": "[dead]", "action": "Move", "target": "Madrid",  "support": 0}, "C":
                    {"army": "C", "location": "[dead]",
                        "action": "Support", "target": "A",  "support": 0},
                    "D": {"army": "D", "location": "[dead]", "action": "Move", "target": "Paris",  "support": 0},
                    "E": {"army": "E", "location": "Rome", "action": "Move", "target": "Rome",  "support": 0}}
        self.assertEqual(army_info, expected)

    # ----
    # print
    # ----

    def test_single_army(self):
        """Test with a single army in a specified location."""
        army_info = {"Army1": {"location": "Paris"}}
        output = StringIO()
        diplomacy_print(output, army_info)
        expected_output = "Army1 Paris\n"
        self.assertEqual(output.getvalue(), expected_output)

    def test_multiple_armies(self):
        army_info = {
            "Army1": {"location": "Berlin"},
            "Army2": {"location": "Paris"},
            "Army3": {"location": "Rome"}
        }
        output = StringIO()
        diplomacy_print(output, army_info)
        expected_output = "Army1 Berlin\nArmy2 Paris\nArmy3 Rome\n"
        self.assertEqual(output.getvalue(), expected_output)

    def test_empty_army_info(self):
        army_info = {}
        output = StringIO()
        diplomacy_print(output, army_info)
        expected_output = ""
        self.assertEqual(output.getvalue(), expected_output)

    def test_army_with_special_characters(self):
        army_info = {
            "Armée Française": {"location": "Paris-Nord"},
            "Heer Österreich": {"location": "Wien"}
        }
        output = StringIO()
        diplomacy_print(output, army_info)
        expected_output = "Armée Française Paris-Nord\nHeer Österreich Wien\n"
        self.assertEqual(output.getvalue(), expected_output)

    # # -----
    # # solve
    # # -----

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_4(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_5(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve_6(self):
        r = StringIO(
            "A Madrid Move Paris\nB Barcelona Move London\nC London Hold\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_7(self):
        r = StringIO(
            "A Madrid Move London\nB Barcelona Move Madrid\nC London Support B\nD Paris Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A London\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_8(self):
        r = StringIO(
            "A Madrid Support B\nB Barcelona Move Paris\nC London Support D\nD Paris Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC London\nD [dead]\n")

    def test_solve_9(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve_10(self):
        r = StringIO(
            "C Paris Support A\nB Barcelona Move Paris\nA Madrid Move Barcelona A\nD London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB Paris\nC [dead]\nD London\n")

    def test_solve_11(self):
        r = StringIO(
            "D Paris Support B\nC Barcelona Hold\nB Austin Move Barcelona\nA Madrid Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB Barcelona\nC [dead]\nD Paris\n")

    # ----
    # main
    # ----


if __name__ == "__main__":  # pragma: no cover
    main()
